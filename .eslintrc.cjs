module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['airbnb', 'airbnb/hooks', 'plugin:prettier/recommended'],
  overrides: [],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react', 'prettier'],
  rules: {
    'prettier/prettier': 'warn',
    'eol-last': 'warn',
    'no-unused-vars': 'warn',
    'prefer-const': 'warn',
    'no-trailing-spaces': 'warn',
    'no-multiple-empty-lines': 'warn',
    semi: 'warn',
    'padded-blocks': 'warn',
    'arrow-parens': 'warn',
    'max-len': 'warn',
    quotes: 'warn',
    'import/prefer-default-export': 'warn',
    'react/prop-types': 'off',
    indent: 'off',
  },
};
