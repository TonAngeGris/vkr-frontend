import * as Yup from 'yup';

export default Yup.object({
  smoking: Yup.bool(),
  foreignPassport: Yup.bool(),
  readyForMoving: Yup.bool(),
  readyForTraveling: Yup.bool(),
  haveChildren: Yup.bool(),
  driverLicence: Yup.bool(),
  nonconvictionCertificate: Yup.bool(),
  medicineCertificate: Yup.bool(),
  mentalStateCertificate: Yup.bool(),
  medicalTests: Yup.bool(),
  canSwimming: Yup.bool(),
});
