import * as Yup from 'yup';

export default Yup.object({
  comment: Yup.string()
    .matches(/^[а-яА-Я -,.!:;+_]+$/, 'Можно использовать только кириллицу')
    .min(10, 'Минимальная длина комментария 10 символов')
    .max(100, 'Максимальная длина комментарий 100 символов')
    .required('Это обязательное поле'),
  rating: Yup.number().required('Это обязательное поле'),
});
