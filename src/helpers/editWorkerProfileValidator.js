import * as Yup from 'yup';

export default Yup.object({
  lastName: Yup.string()
    .matches(/^[а-яА-Я -]+$/, 'Можно использовать только кириллицу')
    .max(50, 'Максимальная длина фамилии 50 символов')
    .required('Это обязательное поле'),
  firstName: Yup.string()
    .matches(/^[а-яА-Я -]+$/, 'Можно использовать только кириллицу')
    .max(50, 'Максимальная длина имени 50 символов')
    .required('Это обязательное поле'),
  patronymic: Yup.string()
    .matches(/^[а-яА-Я -]+$/, 'Можно использовать только кириллицу')
    .max(50, 'Максимальная длина отчества 50 символов'),
  city: Yup.string()
    .matches(/^[а-яА-Я \-,.]+$/, 'Можно использовать только кириллицу')
    .required('Это обязательное поле'),
  phoneNumber: Yup.string()
    .matches(/^\+7[0-9]+$/, 'Неправильный формат номера')
    .length(12, 'Неправильный формат номера')
    .required('Это обязательное поле'),
  age: Yup.number()
    .typeError('Возраст может быть только числом')
    .required('Это обязательное поле')
    .min(16, 'Вам должно быть минимум 16 лет')
    .max(200, 'Вам не может быть больше 200 лет'),
  totalExperience: Yup.number()
    .typeError('Стаж может быть только числом')
    .required('Это обязательное поле')
    .lessThan(
      Yup.ref('age'),
      'Ваш стаж не может быть больше или равен вашему возрасту'
    ),
  password: Yup.string()
    .min(6, 'Минимальная длина пароля 6 символов')
    .max(128, 'Максимальная длина пароля 128 символов')
    .test('passwords-match', 'Пароли не совпадают', function check(value) {
      return (
        !this.parent.repeatedPassword || value === this.parent.repeatedPassword
      );
    }),
  repeatedPassword: Yup.string().test(
    'passwords-match',
    'Пароли не совпадают',
    function check(value) {
      return !this.parent.password || value === this.parent.password;
    }
  ),
});
