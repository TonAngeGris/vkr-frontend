import * as Yup from 'yup';

export default Yup.object({
  title: Yup.string()
    .matches(/^[а-яА-Я -,.!:;+_]+$/, 'Можно использовать только кириллицу')
    .min(4, 'Минимальная длина заголовка 4 символа')
    .max(32, 'Максимальная длина заголовка 50 символов')
    .required('Это обязательное поле'),
  description: Yup.string()
    .min(10, 'Минимальная длина описания 10 символов')
    .max(512, 'Максимальная длина описания 512 символов')
    .required('Это обязательное поле'),
  role: Yup.string()
    .matches(/^[а-яА-Я -]+$/, 'Можно использовать только кириллицу')
    .required('Это обязательное поле'),
  city: Yup.string()
    .matches(/^[а-яА-Я \-,.]+$/, 'Можно использовать только кириллицу')
    .required('Это обязательное поле'),
  experience: Yup.number()
    .min(0, 'Стаж может быть только положительным числом (или 0)')
    .max(100, 'Ваш стаж не может быть больше 100 лет')
    .typeError('Стаж может быть только числом'),
  ageMin: Yup.number()
    .typeError('Возраст может быть только числом')
    .min(16, 'Минимум 16 лет')
    .max(200, 'Максимум 200 лет')
    .test(
      'ageMin-check',
      'Минимальный возраст не может быть больше максимального',
      function check(value) {
        if (!value || !this.parent.ageMax) {
          return true;
        }

        return value <= this.parent.ageMax;
      }
    ),
  ageMax: Yup.number()
    .typeError('Возраст может быть только числом')
    .min(16, 'Минимум 16 лет')
    .max(200, 'Максимум 200 лет')
    .test(
      'ageMax-check',
      'Максимальный возраст не может быть меньше минимального',
      function check(value) {
        if (!value || !this.parent.ageMin) {
          return true;
        }

        return value >= this.parent.ageMin;
      }
    ),
  wage: Yup.number()
    .typeError('Зарплата может быть только числом')
    .max(1_000_000, 'Максимальная зарплата - 1.000.000 рублей'),
  mondayStartTime: Yup.mixed()
    .nullable()
    .test(
      'mondayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.mondayEndTime !== null) ||
          (value !== null && this.parent.mondayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.mondayEndTime === null ||
              this.parent.mondayEndTime !== null) &&
            value < this.parent.mondayEndTime
          );
        }

        return this.parent.mondayEndTime === null;
      }
    ),
  mondayEndTime: Yup.mixed()
    .nullable()
    .test(
      'mondayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.mondayStartTime !== null) ||
          (value !== null && this.parent.mondayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.mondayStartTime === null ||
              this.parent.mondayStartTime !== null) &&
            value > this.parent.mondayStartTime
          );
        }

        return this.parent.mondayStartTime === null;
      }
    ),

  tuesdayStartTime: Yup.mixed()
    .nullable()
    .test(
      'tuesdayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.tuesdayEndTime !== null) ||
          (value !== null && this.parent.tuesdayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.tuesdayEndTime === null ||
              this.parent.tuesdayEndTime !== null) &&
            value < this.parent.tuesdayEndTime
          );
        }

        return this.parent.tuesdayEndTime === null;
      }
    ),
  tuesdayEndTime: Yup.mixed()
    .nullable()
    .test(
      'tuesdayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.tuesdayStartTime !== null) ||
          (value !== null && this.parent.tuesdayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.tuesdayStartTime === null ||
              this.parent.tuesdayStartTime !== null) &&
            value > this.parent.tuesdayStartTime
          );
        }

        return this.parent.tuesdayStartTime === null;
      }
    ),

  wednesdayStartTime: Yup.mixed()
    .nullable()
    .test(
      'wednesdayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.wednesdayEndTime !== null) ||
          (value !== null && this.parent.wednesdayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.wednesdayEndTime === null ||
              this.parent.wednesdayEndTime !== null) &&
            value < this.parent.wednesdayEndTime
          );
        }

        return this.parent.wednesdayEndTime === null;
      }
    ),
  wednesdayEndTime: Yup.mixed()
    .nullable()
    .test(
      'wednesdayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.wednesdayStartTime !== null) ||
          (value !== null && this.parent.wednesdayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.wednesdayStartTime === null ||
              this.parent.wednesdayStartTime !== null) &&
            value > this.parent.wednesdayStartTime
          );
        }

        return this.parent.wednesdayStartTime === null;
      }
    ),

  thursdayStartTime: Yup.mixed()
    .nullable()
    .test(
      'thursdayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.thursdayEndTime !== null) ||
          (value !== null && this.parent.thursdayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.thursdayEndTime === null ||
              this.parent.thursdayEndTime !== null) &&
            value < this.parent.thursdayEndTime
          );
        }

        return this.parent.thursdayEndTime === null;
      }
    ),
  thursdayEndTime: Yup.mixed()
    .nullable()
    .test(
      'thursdayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.thursdayStartTime !== null) ||
          (value !== null && this.parent.thursdayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.thursdayStartTime === null ||
              this.parent.thursdayStartTime !== null) &&
            value > this.parent.thursdayStartTime
          );
        }

        return this.parent.thursdayStartTime === null;
      }
    ),

  fridayStartTime: Yup.mixed()
    .nullable()
    .test(
      'fridayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.fridayEndTime !== null) ||
          (value !== null && this.parent.fridayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.fridayEndTime === null ||
              this.parent.fridayEndTime !== null) &&
            value < this.parent.fridayEndTime
          );
        }

        return this.parent.fridayEndTime === null;
      }
    ),
  fridayEndTime: Yup.mixed()
    .nullable()
    .test(
      'fridayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.fridayStartTime !== null) ||
          (value !== null && this.parent.fridayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.fridayStartTime === null ||
              this.parent.fridayStartTime !== null) &&
            value > this.parent.fridayStartTime
          );
        }

        return this.parent.fridayStartTime === null;
      }
    ),
  sundayStartTime: Yup.mixed()
    .nullable()
    .test(
      'sundayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.sundayEndTime !== null) ||
          (value !== null && this.parent.sundayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.sundayEndTime === null ||
              this.parent.sundayEndTime !== null) &&
            value < this.parent.sundayEndTime
          );
        }

        return this.parent.sundayEndTime === null;
      }
    ),
  sundayEndTime: Yup.mixed()
    .nullable()
    .test(
      'sundayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.sundayStartTime !== null) ||
          (value !== null && this.parent.sundayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.sundayStartTime === null ||
              this.parent.sundayStartTime !== null) &&
            value > this.parent.sundayStartTime
          );
        }

        return this.parent.sundayStartTime === null;
      }
    ),

  saturdayStartTime: Yup.mixed()
    .nullable()
    .test(
      'saturdayEndTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.saturdayEndTime !== null) ||
          (value !== null && this.parent.saturdayEndTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.saturdayEndTime === null ||
              this.parent.saturdayEndTime !== null) &&
            value < this.parent.saturdayEndTime
          );
        }

        return this.parent.saturdayEndTime === null;
      }
    ),
  saturdayEndTime: Yup.mixed()
    .nullable()
    .test(
      'saturdayStartTime-check',
      'Это обязательное поле',
      function check(value) {
        if (
          (value === 0 && this.parent.saturdayStartTime !== null) ||
          (value !== null && this.parent.saturdayStartTime === 0)
        ) {
          return true;
        }

        if (value !== null) {
          return (
            (this.parent.saturdayStartTime === null ||
              this.parent.saturdayStartTime !== null) &&
            value > this.parent.saturdayStartTime
          );
        }

        return this.parent.saturdayStartTime === null;
      }
    ),
});
