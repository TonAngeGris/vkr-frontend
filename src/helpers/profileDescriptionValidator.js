import * as Yup from 'yup';

export default Yup.object({
  description: Yup.string().max(512, 'Максимальная длина 512 символов'),
});
