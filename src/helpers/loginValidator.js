import * as Yup from 'yup';

export default Yup.object({
  phoneNumber: Yup.string()
    .matches(/^\+7[0-9]+$/, 'Неправильный формат номера')
    .length(12, 'Неправильный формат номера')
    .required('Это обязательное поле'),
  password: Yup.string()
    .min(6, 'Минимальная длина пароля 6 символов')
    .max(128, 'Максимальная длина пароля 128 символов')
    .required('Это обязательное поле'),
});
