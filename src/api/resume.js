import api from './api';

export async function getResumeData(id) {
  const response = await api(`/resumes/${id}`);
  return response.data;
}

export async function getResumes(params) {
  const response = await api('/resumes', { params });
  return response.data;
}

export async function createResume(resume) {
  const response = await api({
    method: 'POST',
    data: resume,
    url: '/resumes',
  });

  return response.data;
}

export async function updateResume(resume, id) {
  const response = await api({
    method: 'POST',
    data: resume,
    url: `/resumes/${id}?_method=PATCH`,
  });

  return response.data;
}

export async function deleteResume(id) {
  const response = await api({
    method: 'DELETE',
    url: `/resumes/${id}`,
  });

  return response.data;
}
