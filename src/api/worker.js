import api from './api';

export async function getWorkerData(id) {
  const response = await api(`/workers/${id}`);
  return response.data;
}

export async function getWorkerContracts(id) {
  const response = await api(`/workers/${id}/contracts`);
  return response.data;
}

export async function getWorkerResumes(id) {
  const response = await api(`/workers/${id}/resumes`);
  return response.data;
}

export async function updateWorker(id, bodyData) {
  const formdata = new FormData();

  // eslint-disable-next-line no-prototype-builtins
  if (bodyData.hasOwnProperty('additionalInfo')) {
    formdata.append('additionalInfo', JSON.stringify(bodyData?.additionalInfo));
  } else {
    Object.entries(bodyData).forEach(([key, value]) => {
      formdata.append(key, value);
    });
  }

  const response = await api({
    method: 'POST',
    data: formdata,
    url: `/workers/${id}?_method=PATCH`,
  });
  return response.data;
}
