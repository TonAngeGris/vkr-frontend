import api from './api';

export async function getVacancyData(id) {
  const response = await api(`/vacancies/${id}`);
  return response.data;
}

export async function getVacancies(params) {
  const response = await api('/vacancies', { params });
  return response.data;
}

export async function createVacancy(vacancy) {
  const response = await api({
    method: 'POST',
    data: vacancy,
    url: '/vacancies',
  });

  return response.data;
}

export async function updateVacancy(vacancy, id) {
  const response = await api({
    method: 'POST',
    data: vacancy,
    url: `/vacancies/${id}?_method=PATCH`,
  });

  return response.data;
}

export async function deleteVacancy(id) {
  const response = await api({
    method: 'DELETE',
    url: `/vacancies/${id}`,
  });

  return response.data;
}
