import api from './api';

// eslint-disable-next-line import/prefer-default-export
export async function getEducations(params) {
  const response = await api('/educations', { params });
  return response.data;
}
