import api from './api';

export async function getEmployerData(id) {
  const response = await api(`/employers/${id}`);
  return response.data;
}

export async function getEmployerContracts(id) {
  const response = await api(`/employers/${id}/contracts`);
  return response.data;
}

export async function getEmployerVacancies(id) {
  const response = await api(`/employers/${id}/vacancies`);
  return response.data;
}

export async function updateEmployer(id, bodyData) {
  const formdata = new FormData();

  Object.entries(bodyData).forEach(([key, value]) => {
    formdata.append(key, value);
  });

  const response = await api({
    method: 'POST',
    data: formdata,
    url: `/employers/${id}?_method=PATCH`,
  });
  return response.data;
}
