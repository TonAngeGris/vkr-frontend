import api from './api';

// eslint-disable-next-line import/prefer-default-export
export async function getLanguages(params) {
  const response = await api('/languages', { params });
  return response.data;
}
