import api from './api';

export async function authUser(user, userGroup, authAction) {
  const response = await api.request({
    method: 'POST',
    data: user,
    url: `/auth/${userGroup}/${authAction}`,
  });

  return response.data;
}

export async function whoIAm(userGroup) {
  const response = await api.request({
    method: 'GET',
    url: `/auth/${userGroup}/whoiam`,
  });

  return response.data;
}

export async function refresh(refreshToken, userGroup) {
  const data = {
    refreshToken,
    userGroup,
  };

  const response = await api.request({
    method: 'POST',
    data,
    url: '/auth/refresh',
  });

  return response.data;
}
