import api from './api';

// eslint-disable-next-line import/prefer-default-export
export async function getCitizenships(params) {
  const response = await api('/citizenships', { params });
  return response.data;
}
