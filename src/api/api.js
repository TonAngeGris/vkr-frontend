import axios from 'axios';

const api = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
});

let apiInterceptor = null;

export function addAccessTokenToHeaders(accessToken) {
  // Отписать слушатель перехватчика
  if (!!apiInterceptor || apiInterceptor === 0) {
    api.interceptors.request.eject(apiInterceptor);
  }

  apiInterceptor = api.interceptors.request.use(
    (config) => {
      const { headers } = config;
      headers.Authorization = `Bearer ${accessToken}`;
      return config;
    },
    (error) => Promise.reject(error)
  );
}

export default api;
