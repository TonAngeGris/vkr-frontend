import api from './api';

// eslint-disable-next-line import/prefer-default-export
export async function getRoles(params) {
  const response = await api('/roles', { params });
  return response.data;
}
