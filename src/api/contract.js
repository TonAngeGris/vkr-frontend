import api from './api';

export async function createContract(contract) {
  const response = await api({
    method: 'POST',
    data: contract,
    url: '/contracts',
  });

  return response.data;
}

export async function deleteContract(id) {
  const response = await api({
    method: 'DELETE',
    url: `/contracts/${id}`,
  });

  return response.data;
}

export async function updateContract(contract, id) {
  const response = await api({
    method: 'POST',
    data: contract,
    url: `/contracts/${id}?_method=PATCH`,
  });

  return response.data;
}
