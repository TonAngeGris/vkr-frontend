import api from './api';

// eslint-disable-next-line import/prefer-default-export
export async function getCities(params) {
  const response = await api('/cities', { params });
  return response.data;
}
