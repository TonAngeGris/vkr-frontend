const REFRESH_TOKEN = 'refreshToken';
const USER_GROUP = 'userGroup';
const SEARCH_PARAMS = 'searchParams';

// Refresh token
export function addRefreshTokenToLS(refreshTokenValue) {
  localStorage.setItem(REFRESH_TOKEN, refreshTokenValue);
}

export function removeRefreshTokenFromLS() {
  localStorage.removeItem(REFRESH_TOKEN);
}

export function readRefreshTokenFromLS() {
  return localStorage.getItem(REFRESH_TOKEN);
}

// User group
export function addUserTypeToLS(userGroup) {
  localStorage.setItem(USER_GROUP, userGroup);
}

export function removeUserTypeFromLS() {
  localStorage.removeItem(USER_GROUP);
}

export function readUserTypeFromLS() {
  return localStorage.getItem(USER_GROUP);
}

// Search params
export function addSearchParamsToLS(params) {
  localStorage.setItem(SEARCH_PARAMS, JSON.stringify(params));
}

export function readSearchParamsFromLS() {
  return JSON.parse(localStorage.getItem(SEARCH_PARAMS));
}

export function removeSearchParamsFromLS() {
  localStorage.removeItem(SEARCH_PARAMS);
}
