import React, { useState } from 'react';
import {
  Button,
  CssBaseline,
  TextField,
  Grid,
  Box,
  Typography,
  Container,
  ButtonGroup,
  Snackbar,
  Alert,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { Link, Navigate } from 'react-router-dom';

import AuthLogo from '../components/AuthLogo';
import loginValidator from '../helpers/loginValidator';
import { requestAuthUser } from '../redux/actions/auth';
import { readUserTypeFromLS } from '../lib/localStorage';

function SignIn() {
  const formik = useFormik({
    initialValues: {
      phoneNumber: '',
      password: '',
    },
    validationSchema: loginValidator,
  });

  const [group, setGroup] = useState('workers');
  const [openSnackbar, setOpenSnackbar] = useState(true);

  const dispatch = useDispatch();

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleSelectGroup = (event) => {
    setGroup(event.target.id);
  };

  const handleSubmitForm = (event) => {
    event.preventDefault();

    const payload = {
      ...formik.values,
      userGroup: group,
      authAction: 'login',
    };

    dispatch(requestAuthUser(payload));
  };

  const { authUser, error: loginRequestError } = useSelector(
    (state) => state.authReducer
  );

  const isAuth = Object.keys(authUser).length;
  const isAuthError = loginRequestError !== null;
  const userGroup = readUserTypeFromLS();

  if (isAuth) {
    return <Navigate to={`/${userGroup}/${authUser.id}`} />;
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: '2.5rem',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <AuthLogo title="Вход" />

        <ButtonGroup disableElevation variant="outlined">
          <Button
            id="workers"
            variant={group === 'workers' ? 'contained' : 'outlined'}
            onClick={handleSelectGroup}
          >
            Работник
          </Button>
          <Button
            id="employers"
            variant={group === 'employers' ? 'contained' : 'outlined'}
            onClick={handleSelectGroup}
          >
            Работодатель
          </Button>
        </ButtonGroup>

        <Box
          component="form"
          onSubmit={handleSubmitForm}
          noValidate
          sx={{ mt: 1 }}
        >
          <TextField
            margin="normal"
            required
            fullWidth
            id="phoneNumber"
            label="Номер телефона"
            name="phoneNumber"
            autoComplete="phone-number"
            helperText={
              formik.touched.phoneNumber ? formik.errors.phoneNumber : ''
            }
            error={
              formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
            }
            value={formik.values.phoneNumber}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Пароль"
            type="password"
            id="password"
            autoComplete="new-password"
            helperText={formik.touched.password ? formik.errors.password : ''}
            error={formik.touched.password && Boolean(formik.errors.password)}
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <Box display="flex" justifyContent="center">
            <Button
              disabled={
                Object.keys(formik.errors).length !== 0 ||
                !formik.values.phoneNumber ||
                !formik.values.password
              }
              type="submit"
              variant="contained"
              sx={{ mt: 2, mb: 2, width: '6.5em' }}
            >
              Войти
            </Button>
          </Box>
          <Grid
            container
            display="flex"
            flexDirection="column"
            alignItems="center"
          >
            <Grid item display="flex" alignItems="center">
              <Typography variant="body2" sx={{ mr: '0.25rem' }}>
                Ещё нет аккаунта?
              </Typography>
              <Link sx={{ cursor: 'pointer' }} to="/sign-up" variant="body2">
                Зарегистрироваться
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>

      {/* TODO: пофиксить ошибку Network error при рефреше БД и обновлении страницы */}
      {isAuthError && (
        <Snackbar
          open={isAuthError && openSnackbar}
          autoHideDuration={5000}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
          onClose={handleCloseSnackbar}
        >
          <Alert
            severity="error"
            sx={{ width: '100%' }}
            onClose={handleCloseSnackbar}
          >
            {loginRequestError}
          </Alert>
        </Snackbar>
      )}
    </Container>
  );
}

export default SignIn;
