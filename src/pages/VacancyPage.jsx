import React, { useEffect, useState } from 'react';
import {
  Alert,
  Box,
  Button,
  Container,
  Divider,
  Grid,
  Rating,
  Snackbar,
  Typography,
} from '@mui/material';
import Image from 'mui-image';
import { grey } from '@mui/material/colors';
import PlaceIcon from '@mui/icons-material/Place';
import PhoneIcon from '@mui/icons-material/Phone';
import { useNavigate, useParams } from 'react-router-dom';

import { useSelector } from 'react-redux';
import VacancyCard from '../components/VacancyCard';
import { getVacancyData } from '../api/vacancy';
import { getEmployerData } from '../api/employer';

const BASE_AVATAR_URL = import.meta.env.VITE_AVATAR_URL;

function VacancyPage() {
  const [vacancyData, setVacancyData] = useState(null);
  const [employerData, setEmployerData] = useState(null);
  const [fetchError, setFetchError] = useState(null);

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchVacancy(id) {
      try {
        const fetchedVacancyData = await getVacancyData(id);
        const { employer_id: employerId } = fetchedVacancyData;
        const fetchedEmployerData = await getEmployerData(employerId);

        setVacancyData(fetchedVacancyData);
        setEmployerData(fetchedEmployerData);
      } catch (err) {
        setFetchError(err.message);
      }
    }

    fetchVacancy(params.id);
  }, [params]);

  const { authUser } = useSelector((state) => state.authReducer);

  const isAuth = Object.keys(authUser).length;

  if (vacancyData && employerData) {
    let ageText = 'год';
    if (employerData.age % 10 !== 1) {
      if (
        [2, 3, 4].includes(employerData.age % 10) &&
        ![10, 11, 12, 13, 14].includes(employerData.age)
      ) {
        ageText = 'года';
      } else {
        ageText = 'лет';
      }
    }

    const num = employerData.phone_number;
    const phoneNumber = `${num.slice(0, 2)} 
     (${num.slice(2, 5)})
      ${num.slice(5, 8)}-${num.slice(8, 10)}-${num.slice(10, 12)}`;

    return (
      <Container maxWidth="lg">
        <Grid container mt="1.5em" mb="2em">
          <Grid item xs={3}>
            <Box>
              <Image
                width="100%"
                height="100%"
                duration={0}
                src={`${BASE_AVATAR_URL}/${employerData.avatar}`}
                style={{
                  borderRadius: '20px',
                  border: `5px ${grey[500]} solid`,
                }}
              />
            </Box>
            <Typography
              mt="0.25em"
              textAlign="center"
              component="h6"
              variant="h6"
            >
              {employerData.lastname} {employerData.firstname}{' '}
              {employerData.patronymic}
            </Typography>
            <Box display="flex" justifyContent="center" mt="0.5em">
              <Typography fontSize="14pt">
                Возраст:{' '}
                <b>
                  {employerData.age} {ageText}
                </b>
              </Typography>
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Typography color={grey[600]} textAlign="center">
                <PlaceIcon
                  sx={{ position: 'relative', top: '0.2em', right: '0.1em' }}
                  fontSize="small"
                />
                <b>{employerData.city.city_name}</b>
              </Typography>
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              {isAuth ? (
                <>
                  <PhoneIcon fontSize="small" />
                  <Typography ml="0.5em" color={grey[600]}>
                    <b>{phoneNumber}</b>
                  </Typography>
                </>
              ) : (
                <Button
                  variant="outlined"
                  size="small"
                  endIcon={<PhoneIcon />}
                  onClick={() => navigate('/sign-in')}
                >
                  Посмотреть
                </Button>
              )}
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Rating
                name="read-only"
                value={Number(employerData.rating)}
                precision={0.25}
                readOnly
              />
              <Typography ml="0.25em" fontWeight="bold" color={grey[600]}>
                {Math.round(Number(employerData.rating) * 10) / 10}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={9} pl="2em">
            <Typography variant="h6">О работодателе</Typography>
            <Typography variant="body1">
              {employerData?.description ?? 'Информация отсутствует'}
            </Typography>

            <Divider sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }} />

            <Typography variant="h6">Вакансия</Typography>
            <Box mt="0.5em" width="33.25%">
              <VacancyCard
                title={vacancyData.title}
                ageMin={vacancyData.age_min}
                ageMax={vacancyData.age_max}
                description={vacancyData.description}
                city={vacancyData.city.city_name}
                experience={vacancyData.experience}
                wage={vacancyData.wage}
                createdAt={vacancyData.created_at}
                role={vacancyData.role.domestic_role_name}
                schedule={vacancyData.schedule}
                employerId={vacancyData.employer_id}
                showMore
              />
            </Box>
          </Grid>
        </Grid>

        <Snackbar open={!!fetchError} autoHideDuration={3000}>
          <Alert severity="error" sx={{ width: '100%' }}>
            {fetchError}
          </Alert>
        </Snackbar>
      </Container>
    );
  }
}

export default VacancyPage;
