import React, { useEffect, useState } from 'react';
import {
  Container,
  Grid,
  Typography,
  Box,
  Divider,
  Rating,
  Snackbar,
  Alert,
  Button,
  IconButton,
} from '@mui/material';
import Image from 'mui-image';
import { grey } from '@mui/material/colors';
import PlaceIcon from '@mui/icons-material/Place';
import PhoneIcon from '@mui/icons-material/Phone';
import { useNavigate, useParams } from 'react-router-dom';
import DriveFileRenameOutlineRoundedIcon from '@mui/icons-material/DriveFileRenameOutlineRounded';
import { useDispatch, useSelector } from 'react-redux';
import AddIcon from '@mui/icons-material/Add';

import VacancyCard from '../components/VacancyCard';
import ContractCard from '../components/ContractCard';
import { updateEmployer } from '../api/employer';
import { fetchUserData, setUserMainData } from '../redux/actions/user';
import { setAuthEditedData } from '../redux/actions/auth';
import Loader from '../components/Loader';
import ProfileEditDialog from '../components/ProfileEditDialog';
import ProfileDescriptionEditDialog from '../components/ProfileDescriptionEditDialog';
import { readUserTypeFromLS } from '../lib/localStorage';
import VacancyCreateDialog from '../components/VacancyCreateDialog';

const cardTextStyles = {
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  display: '-webkit-box',
  WebkitLineClamp: 3,
  WebkitBoxOrient: 'vertical',
};

const BASE_AVATAR_URL = import.meta.env.VITE_AVATAR_URL;
const UPLOAD_ICON = '../../upload-icon.jpg';

function EmployerPage() {
  const [userUpdateError, setUserUpdateError] = useState('');

  // Стейт для открытия модалки изменения основной инфы профиля
  const [profileEditDialogIsOpened, setProfileEditDialogIsOpened] =
    useState(false);

  // Стейт для открытия модалки изменения "О себе"
  const [
    profileDescriptionEditDialogIsOpened,
    setProfileDescriptionEditDialogIsOpened,
  ] = useState(false);

  // Ховер для появления иконки загрузки при наведении на фото профиля
  const [hoverOn, setHoverOn] = useState(false);

  // Стейт для открытия модалки создания вакансии
  const [vacancyCreateDialogIsOpened, setVacancyCreateDialogIsOpened] =
    useState(false);

  // Хендлер для открытия модалки изменения основной инфы профиля
  const handleProfileEditDialogOpen = () => setProfileEditDialogIsOpened(true);
  const handleProfileEditDialogClose = () =>
    setProfileEditDialogIsOpened(false);

  // Хендлер для открытия модалки изменения "О себе"
  const handleProfileDescriptionEditDialogOpen = () =>
    setProfileDescriptionEditDialogIsOpened(true);
  const handleProfileDescriptionEditDialogClose = () =>
    setProfileDescriptionEditDialogIsOpened(false);

  // Хендлер для открытия модалки создания вакансии
  const handleVacancyCreateDialogOpen = () =>
    setVacancyCreateDialogIsOpened(true);
  const handleVacancyCreateDialogClose = () =>
    setVacancyCreateDialogIsOpened(false);

  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleCloseSnackbar = (_, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setUserUpdateError('');
  };

  const handleAvatarUpload = async (event) => {
    const data = await updateEmployer(params.id, {
      avatar: event.target.files[0],
    });

    dispatch(setUserMainData({ userData: data, userGroup: 'employer' }));
    dispatch(setAuthEditedData(data));
  };

  const { authUser } = useSelector((state) => state.authReducer);

  const {
    userData,
    fetching: isUserFetching,
    error: isUserFetchError,
  } = useSelector((state) => state.userReducer);

  const isAuth = Object.keys(authUser).length;
  const isUserFetched =
    userData?.employerData && Object.keys(userData?.employerData).length;
  const userGroup = readUserTypeFromLS();
  const isAccountOwnerAuth =
    userGroup === 'employers' && authUser.id === userData?.employerData?.id;

  const handleClickImage = () => {
    if (isAccountOwnerAuth) {
      document.getElementById('file-upload').click();
    }
  };

  const handleOnMouseEnter = () => {
    if (isAccountOwnerAuth) {
      setHoverOn(true);
    }
  };

  const handleOnMouseLeave = () => {
    if (isAccountOwnerAuth) {
      setHoverOn(false);
    }
  };

  useEffect(() => {
    const data = {
      id: params.id,
      userGroup: 'employers',
    };

    dispatch(fetchUserData(data));
  }, [params.id, dispatch]);

  if (isUserFetching) {
    return <Loader text="Загрузка профиля..." />;
  }

  if (isUserFetched && !isUserFetching) {
    const { employerData, employerVacancies, employerContracts } = userData;

    let ageText = 'год';
    if (employerData.age % 10 !== 1) {
      if (
        [2, 3, 4].includes(employerData.age % 10) &&
        ![10, 11, 12, 13, 14].includes(employerData.age)
      ) {
        ageText = 'года';
      } else {
        ageText = 'лет';
      }
    }

    const num = employerData.phone_number;
    const phoneNumber = `${num.slice(0, 2)} 
     (${num.slice(2, 5)})
      ${num.slice(5, 8)}-${num.slice(8, 10)}-${num.slice(10, 12)}`;

    return (
      <Container
        sx={{
          maxWidth: '100%',
        }}
      >
        <Grid container mt="1.5em" mb="2em">
          <Grid item xs={3}>
            <Box position="relative" width="100%" pb="100%">
              <Image
                duration={0}
                src={
                  !hoverOn ? BASE_AVATAR_URL + employerData.avatar : UPLOAD_ICON
                }
                style={{
                  top: '0',
                  position: 'absolute',
                  objectFit: 'cover',
                  width: '100%',
                  height: '100%',
                  borderRadius: '20px',
                  border: `5px ${grey[500]} solid`,
                }}
                onClick={handleClickImage}
                onMouseEnter={handleOnMouseEnter}
                onMouseLeave={handleOnMouseLeave}
              />
              <input
                hidden
                accept="image/*"
                type="file"
                id="file-upload"
                onChange={handleAvatarUpload}
              />
            </Box>
            <Typography
              mt="0.25em"
              textAlign="center"
              component="h6"
              variant="h6"
            >
              {employerData.lastname} {employerData.firstname}{' '}
              {employerData?.patronymic}
            </Typography>
            <Box display="flex" justifyContent="center" mt="0.5em">
              <Typography fontSize="14pt">
                Возраст:{' '}
                <b>
                  {employerData.age} {ageText}
                </b>
              </Typography>
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Typography color={grey[600]} textAlign="center">
                <PlaceIcon
                  sx={{ position: 'relative', top: '0.2em', right: '0.1em' }}
                  fontSize="small"
                />
                <b>{employerData.city?.city_name}</b>
              </Typography>
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Box display="flex" alignItems="center">
                {isAuth ? (
                  <>
                    <PhoneIcon fontSize="small" />
                    <Typography ml="0.5em" color={grey[600]}>
                      <b>{phoneNumber}</b>
                    </Typography>
                  </>
                ) : (
                  <Button
                    variant="outlined"
                    size="small"
                    endIcon={<PhoneIcon />}
                    onClick={() => navigate('/sign-in')}
                  >
                    Посмотреть
                  </Button>
                )}
              </Box>
            </Box>

            {isAccountOwnerAuth && (
              <Box
                display="flex"
                color="primary.darkgrey"
                mt="0.5em"
                justifyContent="center"
              >
                <Button
                  size="small"
                  variant="outlined"
                  onClick={handleProfileEditDialogOpen}
                  startIcon={<DriveFileRenameOutlineRoundedIcon />}
                >
                  Редактировать
                </Button>
              </Box>
            )}

            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Rating
                name="read-only"
                value={Number(employerData?.rating)}
                precision={0.25}
                readOnly
              />
              <Typography ml="0.25em" fontWeight="bold" color={grey[600]}>
                {Math.round(Number(employerData?.rating) * 10) / 10} (
                {employerContracts.completed?.length})
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={9} pl="2em">
            <Box display="flex" alignItems="center" justifyContent="start">
              <Typography mr="0.3em" variant="h6">
                О себе
              </Typography>
              {isAccountOwnerAuth && (
                <IconButton
                  color="primary"
                  component="label"
                  onClick={handleProfileDescriptionEditDialogOpen}
                >
                  <DriveFileRenameOutlineRoundedIcon bgcolor="primary" />
                </IconButton>
              )}
            </Box>

            <Typography
              variant="body1"
              whiteSpace="break"
              sx={{ hyphens: 'auto', wordBreak: 'break-word' }}
            >
              {employerData?.description ?? 'Информация отсутствует'}
            </Typography>

            <Divider sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }} />

            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography variant="h6">Предлагаемые вакансии</Typography>
              {isAccountOwnerAuth && (
                <Button
                  size="small"
                  variant="outlined"
                  startIcon={<AddIcon />}
                  onClick={handleVacancyCreateDialogOpen}
                >
                  Добавить
                </Button>
              )}
            </Box>

            {employerVacancies?.length ? (
              <Box mt="0.5em" width="33.25%">
                {employerVacancies.map((item) => (
                  <VacancyCard
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    ageMin={item.age_min}
                    ageMax={item.age_max}
                    description={item.description}
                    city={item.city.city_name}
                    experience={item.experience}
                    wage={item.wage}
                    createdAt={item.created_at}
                    role={item.role.domestic_role_name}
                    schedule={item.schedule}
                    descriptionTextStyles={cardTextStyles}
                    additionalInfo={item.additional_info}
                    isAccountOwnerAuth={isAccountOwnerAuth}
                  />
                ))}
              </Box>
            ) : (
              <Typography variant="body1">Отсутствуют</Typography>
            )}

            {/* TODO: сортировка, сначала те, на которые пришли запросы */}
            {isAccountOwnerAuth && (
              <>
                <Divider
                  sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }}
                />
                <Typography variant="h6">
                  Договоры, ожидающие подтверждение
                </Typography>
                {employerContracts.pending_assignment.length ? (
                  <Box mt="0.5em" width="33.25%">
                    {employerContracts.pending_assignment.map((item) => (
                      <ContractCard
                        isEmployer
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        city={item.city.city_name}
                        wage={item.wage}
                        role={item.role.domestic_role_name}
                        startDate={item.start_date}
                        endDate={item.end_date}
                        workerAssigned={item.worker_assigned}
                        employerAssigned={item.employer_assigned}
                        workerCompletionAgreement={
                          item.worker_completion_agreement
                        }
                        employerCompletionAgreement={
                          item.employer_completion_agreement
                        }
                        workerRatingByEmployer={item.worker_rating}
                        employerRatingByWorker={item.employer_rating}
                        workerComment={item.worker_comment}
                        employerComment={item.employer_comment}
                        workerId={item.worker.id}
                        workerFirstName={item.worker.firstname}
                        workerLastName={item.worker.lastname}
                        color="primary"
                      />
                    ))}
                  </Box>
                ) : (
                  <Typography variant="body1">Отсутствуют</Typography>
                )}
              </>
            )}

            {/* TODO: сортировка, сначала те, на которые пришли запросы */}
            {isAccountOwnerAuth && (
              <>
                <Divider
                  sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }}
                />
                <Typography variant="h6">
                  Договоры, ожидающие завершение
                </Typography>
                {employerContracts.pending_completion.length ? (
                  <Box mt="0.5em" width="33.25%">
                    {employerContracts.pending_completion.map((item) => (
                      <ContractCard
                        isEmployer
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        city={item.city.city_name}
                        wage={item.wage}
                        role={item.role.domestic_role_name}
                        startDate={item.start_date}
                        endDate={item.end_date}
                        workerAssigned={item.worker_assigned}
                        employerAssigned={item.employer_assigned}
                        workerCompletionAgreement={
                          item.worker_completion_agreement
                        }
                        employerCompletionAgreement={
                          item.employer_completion_agreement
                        }
                        workerRatingByEmployer={item.worker_rating}
                        employerRatingByWorker={item.employer_rating}
                        workerComment={item.worker_comment}
                        employerComment={item.employer_comment}
                        employerId={item.employer.id}
                        workerId={item.worker.id}
                        workerFirstName={item.worker.firstname}
                        workerLastName={item.worker.lastname}
                        color="primary"
                      />
                    ))}
                  </Box>
                ) : (
                  <Typography variant="body1">Отсутствуют</Typography>
                )}
              </>
            )}

            <Divider sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }} />

            <Typography variant="h6">Действующие договоры</Typography>
            {employerContracts.in_progress.length ? (
              <Box mt="0.5em" width="33.25%">
                {employerContracts.in_progress.map((item) => (
                  <ContractCard
                    isEmployer
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    city={item.city.city_name}
                    wage={item.wage}
                    role={item.role.domestic_role_name}
                    startDate={item.start_date}
                    endDate={item.end_date}
                    workerAssigned={item.worker_assigned}
                    employerAssigned={item.employer_assigned}
                    workerCompletionAgreement={item.worker_completion_agreement}
                    employerCompletionAgreement={
                      item.employer_completion_agreement
                    }
                    workerRatingByEmployer={item.worker_rating}
                    employerRatingByWorker={item.employer_rating}
                    workerComment={item.worker_comment}
                    employerComment={item.employer_comment}
                    employerId={item.employer.id}
                    workerId={item.worker.id}
                    workerFirstName={item.worker.firstname}
                    workerLastName={item.worker.lastname}
                    color="primary"
                  />
                ))}
              </Box>
            ) : (
              <Typography variant="body1">Отсутствуют</Typography>
            )}

            <Divider sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }} />

            <Typography variant="h6">Завершённые договоры</Typography>
            {employerContracts.completed.length ? (
              <Box mt="0.5em" width="33.25%">
                {employerContracts.completed.map((item) => (
                  <ContractCard
                    isEmployer
                    key={item.id}
                    title={item.title}
                    city={item.city.city_name}
                    wage={item.wage}
                    role={item.role.domestic_role_name}
                    startDate={item.start_date}
                    endDate={item.end_date}
                    workerAssigned={item.worker_assigned}
                    employerAssigned={item.employer_assigned}
                    workerCompletionAgreement={item.worker_completion_agreement}
                    employerCompletionAgreement={
                      item.employer_completion_agreement
                    }
                    workerRatingByEmployer={item.worker_rating}
                    employerRatingByWorker={item.employer_rating}
                    workerComment={item.worker_comment}
                    employerComment={item.employer_comment}
                    workerId={item.worker.id}
                    workerFirstName={item.worker.firstname}
                    workerLastName={item.worker.lastname}
                    color="primary"
                  />
                ))}
              </Box>
            ) : (
              <Typography variant="body1">Отсутствуют</Typography>
            )}
          </Grid>
        </Grid>

        <Snackbar
          open={isUserFetchError || Boolean(userUpdateError)}
          autoHideDuration={5000}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
          onClose={handleCloseSnackbar}
        >
          <Alert
            severity="error"
            sx={{ width: '100%' }}
            onClose={handleCloseSnackbar}
          >
            {isUserFetchError || userUpdateError}
          </Alert>
        </Snackbar>

        <ProfileEditDialog
          open={profileEditDialogIsOpened}
          handleClose={handleProfileEditDialogClose}
          color="primary"
          userData={employerData}
          setError={setUserUpdateError}
          userGroup="employer"
        />

        <ProfileDescriptionEditDialog
          open={profileDescriptionEditDialogIsOpened}
          handleClose={handleProfileDescriptionEditDialogClose}
          color="primary"
          userDescription={employerData?.description}
          setError={setUserUpdateError}
          userGroup="employer"
        />

        <VacancyCreateDialog
          open={vacancyCreateDialogIsOpened}
          handleClose={handleVacancyCreateDialogClose}
          color="primary"
          setError={setUserUpdateError}
          employerId={authUser.id}
        />
      </Container>
    );
  }
}

export default EmployerPage;
