import {
  Alert,
  Box,
  Breadcrumbs,
  Button,
  Container,
  Divider,
  Grid,
  Rating,
  Snackbar,
  Typography,
} from '@mui/material';
import Image from 'mui-image';
import React, { useEffect, useState } from 'react';
import { grey } from '@mui/material/colors';
import PlaceIcon from '@mui/icons-material/Place';
import PhoneIcon from '@mui/icons-material/Phone';
import { useNavigate, useParams } from 'react-router-dom';

import { useSelector } from 'react-redux';
import ResumeCard from '../components/ResumeCard';
import { getResumeData } from '../api/resume';
import { getWorkerData } from '../api/worker';
import AdditionalInfo from '../components/AdditionalInfo';

const BASE_AVATAR_URL = import.meta.env.VITE_AVATAR_URL;

function ResumePage() {
  const [resumeData, setResumeData] = useState(null);
  const [workerData, setWorkerData] = useState(null);
  const [fetchError, setFetchError] = useState(null);

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchResume(id) {
      try {
        const fetchedResumeData = await getResumeData(id);
        const { worker_id: workerId } = fetchedResumeData;
        const fetchedWorkerData = await getWorkerData(workerId);

        setResumeData(fetchedResumeData);
        setWorkerData(fetchedWorkerData);
      } catch (err) {
        setFetchError(err.message);
      }
    }

    fetchResume(params.id);
  }, [params]);

  const { authUser } = useSelector((state) => state.authReducer);

  const isAuth = Object.keys(authUser).length;

  if (resumeData && workerData) {
    let ageText = 'год';
    if (workerData.age % 10 !== 1) {
      if (
        [2, 3, 4].includes(workerData.age % 10) &&
        ![10, 11, 12, 13, 14].includes(workerData.age)
      ) {
        ageText = 'года';
      } else {
        ageText = 'лет';
      }
    }

    let totalExperienceText = 'год';
    if (workerData.total_experience % 10 !== 1) {
      if ([2, 3, 4].includes(workerData.total_experience % 10)) {
        totalExperienceText = 'года';
      } else {
        totalExperienceText = 'лет';
      }
    }

    const num = workerData.phone_number;
    const phoneNumber = `${num.slice(0, 2)} 
     (${num.slice(2, 5)})
      ${num.slice(5, 8)}-${num.slice(8, 10)}-${num.slice(10, 12)}`;

    return (
      <Container maxWidth="lg">
        <Grid container mt="1.5em" mb="2em">
          <Grid item xs={3}>
            <Box>
              <Image
                width="100%"
                height="100%"
                duration={0}
                src={BASE_AVATAR_URL + workerData.avatar}
                style={{
                  borderRadius: '20px',
                  border: `5px ${grey[500]} solid`,
                }}
              />
            </Box>
            <Typography
              mt="0.25em"
              textAlign="center"
              component="h6"
              variant="h6"
            >
              {workerData.lastname} {workerData.firstname}{' '}
              {workerData.patronymic}
            </Typography>
            <Box display="flex" justifyContent="center" mt="0.5em">
              <Breadcrumbs separator="|" aria-label="breadcrumb">
                <Typography fontSize="14pt">
                  Возраст:{' '}
                  <b>
                    {workerData.age} {ageText}
                  </b>
                </Typography>
                <Typography fontSize="14pt">
                  Опыт:{' '}
                  <b>
                    {workerData?.total_experience} {totalExperienceText}
                  </b>
                </Typography>
              </Breadcrumbs>
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Typography color={grey[600]} textAlign="center">
                <PlaceIcon
                  sx={{ position: 'relative', top: '0.2em', right: '0.1em' }}
                  fontSize="small"
                />
                <b>{workerData.city.city_name}</b>
              </Typography>
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              {isAuth ? (
                <>
                  <PhoneIcon fontSize="small" />
                  <Typography ml="0.5em" color={grey[600]}>
                    <b>{phoneNumber}</b>
                  </Typography>
                </>
              ) : (
                <Button
                  variant="outlined"
                  color="secondary"
                  size="small"
                  endIcon={<PhoneIcon />}
                  onClick={() => navigate('/sign-in')}
                >
                  Посмотреть
                </Button>
              )}
            </Box>
            <Box
              display="flex"
              color="primary.darkgrey"
              mt="0.5em"
              justifyContent="center"
            >
              <Rating
                name="read-only"
                value={Number(workerData.rating)}
                precision={0.25}
                readOnly
              />
              <Typography ml="0.25em" fontWeight="bold" color={grey[600]}>
                {Math.round(Number(workerData.rating) * 10) / 10}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={9} pl="2em">
            <Typography variant="h6">О работнике</Typography>
            <Typography variant="body1">{workerData.description}</Typography>

            <Divider sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }} />

            <Typography variant="h6">Резюме</Typography>
            <Box mt="0.5em" width="33.25%">
              <ResumeCard
                title={resumeData.title}
                description={resumeData.description}
                city={resumeData.city.city_name}
                experience={resumeData.experience}
                wage={resumeData.wage}
                createdAt={resumeData.created_at}
                role={resumeData.role.domestic_role_name}
                schedule={resumeData.schedule}
                workerId={workerData.id}
                showMore
              />
            </Box>

            <Divider sx={{ mt: '1em', mb: '0.5em', borderBottomWidth: 3 }} />

            <AdditionalInfo isAuth={isAuth} workerData={workerData} />
          </Grid>
        </Grid>

        <Snackbar open={!!fetchError} autoHideDuration={3000}>
          <Alert severity="error" sx={{ width: '100%' }}>
            {fetchError}
          </Alert>
        </Snackbar>
      </Container>
    );
  }
}

export default ResumePage;
