import React, { useEffect, useRef, useState } from 'react';
import {
  Container,
  Grid,
  Box,
  Typography,
  Button,
  Pagination,
  Snackbar,
  Alert,
  CircularProgress,
} from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';
import { useDispatch, useSelector } from 'react-redux';

import RangeAccordion from '../components/RangeAccordion';
import TextFieldAccordion from '../components/TextFieldAccordion';
import SelectAccordion from '../components/SelectAccordion';
import TimePickerAccordion from '../components/TimePickerAccordion';
import ResumeCard from '../components/ResumeCard';
import {
  addSearchParamsToLS,
  readSearchParamsFromLS,
  removeSearchParamsFromLS,
} from '../lib/localStorage';
import { fetchResumes } from '../redux/actions/resumes';
import SearchField from '../components/SearchField';
import AutocompleteAccordion from '../components/AutocompleteAccordion';
import { getCities } from '../api/city';

const sortOptions = [
  { text: 'городам', type: 'city' },
  { text: 'опыту', type: 'experience' },
  { text: 'зарплате', type: 'wage' },
  { text: 'рейтингу', type: 'rating' },
];

const roles = [
  'Все специальности',
  'Горничная',
  'Повар',
  'Няня',
  'Сиделка',
  'Садовник',
  'Водитель',
  'Управляющий',
  'Репетитор',
  'Охранник',
  'Уборщик',
  'Дворецкий',
];

const daysOfWeek = {
  monday: 'Пн',
  tuesday: 'Вт',
  wednesday: 'Ср',
  thursday: 'Чт',
  friday: 'Пт',
  sunday: 'Сб',
  saturday: 'Вс',
};

const cardTextStyles = {
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  display: '-webkit-box',
  WebkitLineClamp: 3,
  WebkitBoxOrient: 'vertical',
};

const CARD_LIMIT = 5;

function Resumes() {
  // Квери параметры
  const [page, setPage] = useState(1);

  // Диапазон возраста
  const [age, setAge] = useState({ min: '', max: '' });

  // Диапазон зарплаты
  const [wage, setWage] = useState({ min: '', max: '' });

  // Опция сортировки
  const [sortOption, setSortOption] = useState(null);

  // Фильтр по городу
  const [city, setCity] = useState('');

  // Выбор роли работника
  const [role, setRole] = useState('');

  // Минимальный опыт работы
  const [experience, setExperience] = useState('');

  // Поле поиска
  const [searchText, setSearchText] = useState('');

  // Выбор расписания
  const [schedule, setSchedule] = useState({
    monday: { startTime: null, endTime: null },
    tuesday: { startTime: null, endTime: null },
    wednesday: { startTime: null, endTime: null },
    thursday: { startTime: null, endTime: null },
    friday: { startTime: null, endTime: null },
    sunday: { startTime: null, endTime: null },
    saturday: { startTime: null, endTime: null },
  });

  const dispatch = useDispatch();

  const handleResetFiltering = () => {
    // setSearchText('');
    setAge({ min: '', max: '' });
    setWage({ min: '', max: '' });
    setSortOption(null);
    setCity('');
    setRole('');
    setExperience('');
    setSchedule({
      monday: { startTime: null, endTime: null },
      tuesday: { startTime: null, endTime: null },
      wednesday: { startTime: null, endTime: null },
      thursday: { startTime: null, endTime: null },
      friday: { startTime: null, endTime: null },
      sunday: { startTime: null, endTime: null },
      saturday: { startTime: null, endTime: null },
    });
    setPage(1);

    removeSearchParamsFromLS();
    dispatch(fetchResumes({ searchText }));
  };

  // При изменении номера страницы:
  // 1. записываем её в стейт
  // 2. берём параметры последнего запроса из LS
  // 3. делаем запрос с изменённой страницей и теми же сохранёнными параметрами
  // 4. сохраняем изменённую страницу в LS
  const handlePagination = (_, value) => {
    setPage(value);

    const params = readSearchParamsFromLS();
    dispatch(fetchResumes({ ...params, page: value }));
    addSearchParamsToLS({ ...params, page: value });
  };

  const {
    items: fetchedResumes,
    allItemsNumber: resumesNumber,
    error: resumesFetchError,
    fetching: areResumesFetching,
  } = useSelector((state) => state.resumesReducer);

  // Проверка на монтирование
  // 2 из-за того, что сначала происходит монтирование, но в его юзэффекте изменяется стейт
  // т.е. происходит перерендер и вызывается юзэффект на обновление, что нужно предотвратить
  const onMount = useRef(2);

  // Поиск при при монтировании:
  // При монтировании берём все параметры из LS и делаем с ними запрос
  // после чего сохраняем взятые из LS параметры в стейты
  useEffect(() => {
    const params = readSearchParamsFromLS() ?? {};
    const {
      searchText: searchTextParam,
      page: pageParam,
      ageMax,
      ageMin,
      role: roleParam,
      city: cityParam,
      experience: experienceParam,
      wageMax,
      wageMin,
      mondayStartTime,
      mondayEndTime,
      tuesdayStartTime,
      tuesdayEndTime,
      wednesdayStartTime,
      wednesdayEndTime,
      thursdayStartTime,
      thursdayEndTime,
      fridayStartTime,
      fridayEndTime,
      sundayStartTime,
      sundayEndTime,
      saturdayStartTime,
      saturdayEndTime,
      sort,
    } = params;

    onMount.current = 2;
    dispatch(fetchResumes(params));

    setSearchText(searchTextParam ?? '');
    setPage(pageParam ?? 1);
    setAge({ min: ageMin ?? '', max: ageMax ?? '' });
    setWage({ min: wageMin ?? '', max: wageMax ?? '' });
    setSortOption(sort ?? null);
    setCity(cityParam ?? '');
    setRole(roleParam ?? '');
    setExperience(experienceParam ?? '');
    setSchedule({
      monday: {
        startTime: mondayStartTime ?? null,
        endTime: mondayEndTime ?? null,
      },
      tuesday: {
        startTime: tuesdayStartTime ?? null,
        endTime: tuesdayEndTime ?? null,
      },
      wednesday: {
        startTime: wednesdayStartTime ?? null,
        endTime: wednesdayEndTime ?? null,
      },
      thursday: {
        startTime: thursdayStartTime ?? null,
        endTime: thursdayEndTime ?? null,
      },
      friday: {
        startTime: fridayStartTime ?? null,
        endTime: fridayEndTime ?? null,
      },
      sunday: {
        startTime: sundayStartTime ?? null,
        endTime: sundayEndTime ?? null,
      },
      saturday: {
        startTime: saturdayStartTime ?? null,
        endTime: saturdayEndTime ?? null,
      },
    });

    // Удалять параметры фильтрации из LS при выходе со страницы
    return () => {
      return removeSearchParamsFromLS();
    };
  }, [dispatch]);

  // Поиск при изменении фильтрации
  useEffect(() => {
    if (onMount.current <= 0) {
      const params = {
        searchText,
        page: 1,
        ageMax: age.max,
        ageMin: age.min,
        role,
        city,
        experience,
        wageMax: wage.max,
        wageMin: wage.min,
        mondayStartTime: schedule.monday.startTime,
        mondayEndTime: schedule.monday.endTime,
        tuesdayStartTime: schedule.tuesday.startTime,
        tuesdayEndTime: schedule.tuesday.endTime,
        wednesdayStartTime: schedule.wednesday.startTime,
        wednesdayEndTime: schedule.wednesday.endTime,
        thursdayStartTime: schedule.thursday.startTime,
        thursdayEndTime: schedule.thursday.endTime,
        fridayStartTime: schedule.friday.startTime,
        fridayEndTime: schedule.friday.endTime,
        sundayStartTime: schedule.sunday.startTime,
        sundayEndTime: schedule.sunday.endTime,
        saturdayStartTime: schedule.saturday.startTime,
        saturdayEndTime: schedule.saturday.endTime,
        sort: sortOption,
      };

      // Т.к. обновляется фильтр,
      // и тут по-хорошему нужно перейти на первую страницу фильтрованных данных
      setPage(1);
      dispatch(fetchResumes(params));
      addSearchParamsToLS(params);
    }

    onMount.current = onMount.current > 0 ? onMount.current - 1 : 0;
  }, [
    dispatch,
    age,
    city,
    experience,
    role,
    wage,
    schedule,
    sortOption,
    searchText,
  ]);

  if (fetchedResumes) {
    return (
      <Container component="main" maxWidth="xl">
        <Grid container spacing={2}>
          <Grid item xs={3.5}>
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              my="1em"
              py="0.5em"
              bgcolor="primary.grey"
              borderRadius="10px"
            >
              <Typography component="h6" variant="h6">
                Расширенный поиск
              </Typography>

              <Button
                pb="0.5em"
                size="small"
                variant="text"
                color="secondary"
                startIcon={<ClearIcon />}
                onClick={handleResetFiltering}
              >
                Сбросить фильтр
              </Button>

              {/* TODO: добавить debounce для всех полей ввода (вместе, желательно) */}
              <Box>
                <SelectAccordion
                  title="Специальность"
                  items={roles}
                  value={role}
                  setValue={setRole}
                  color="secondary"
                />
                <AutocompleteAccordion
                  title="Город"
                  value={city}
                  setValue={setCity}
                  fetchValues={getCities}
                  color="secondary"
                />
                <TimePickerAccordion
                  title="График работы"
                  value={schedule}
                  shortcuts={daysOfWeek}
                  setValue={setSchedule}
                  color="secondary"
                />
                <RangeAccordion
                  title="Возраст"
                  value={age}
                  setValue={setAge}
                  color="secondary"
                />
                <TextFieldAccordion
                  title="Опыт работы"
                  description="Минимум лет"
                  value={experience}
                  setValue={setExperience}
                  color="secondary"
                />
                <RangeAccordion
                  title="Зарплата"
                  value={wage}
                  setValue={setWage}
                  color="secondary"
                />
              </Box>
            </Box>
          </Grid>
          <Grid item xs={8.5}>
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              mt="0.75em"
            >
              <Box
                display="flex"
                flexDirection="column"
                justifyContent="center"
                width="90%"
              >
                <SearchField
                  label="Поиск по заголовку и описанию вакансии"
                  setValue={setSearchText}
                  color="secondary"
                />

                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                  mb="0.5em"
                >
                  <Typography component="h6" variant="h6">
                    Найдено {resumesNumber} резюме
                  </Typography>
                  <Box display="flex" alignItems="center">
                    <Typography component="p" variant="h6" mr="0.25em">
                      Сортировка по:
                    </Typography>
                    {sortOptions.map((item, idx) => (
                      <Box
                        key={item.type}
                        display="inherit"
                        alignItems="inherit"
                      >
                        <Typography component="p" variant="body1" mt="0.2em">
                          {idx !== 0 ? ', ' : ''}
                        </Typography>
                        <Typography
                          component="p"
                          variant="body1"
                          ml="0.2em"
                          color="secondary.main"
                          onClick={() => setSortOption(item.type)}
                          sx={{
                            cursor: 'pointer',
                          }}
                        >
                          {item.type === sortOption ? (
                            <b>{item.text}</b>
                          ) : (
                            item.text
                          )}
                        </Typography>
                      </Box>
                    ))}
                  </Box>
                </Box>
              </Box>

              {resumesNumber > CARD_LIMIT && (
                <Pagination
                  sx={{ mb: '0.75em' }}
                  page={page ?? 0}
                  count={Math.ceil(resumesNumber / CARD_LIMIT)}
                  color="secondary"
                  onChange={handlePagination}
                />
              )}

              {!areResumesFetching ? (
                <>
                  <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    width="30%"
                  >
                    {!resumesNumber ? (
                      <Typography mt="5em" variant="title">
                        Ничего не найдено
                      </Typography>
                    ) : (
                      <>
                        {fetchedResumes.map((item) => (
                          <ResumeCard
                            key={item.id}
                            id={item.id}
                            title={item.title}
                            description={item.description}
                            city={item.city.city_name}
                            age={item.worker.age}
                            experience={item.experience}
                            wage={item.wage}
                            workerId={item.worker.id}
                            workerFirstName={item.worker.firstname}
                            workerLastName={item.worker.lastname}
                            createdAt={item.created_at}
                            rating={item.worker.rating}
                            schedule={item.schedule}
                            role={item.role.domestic_role_name}
                            descriptionTextStyles={cardTextStyles}
                          />
                        ))}
                      </>
                    )}
                  </Box>

                  {resumesNumber > CARD_LIMIT &&
                    page * CARD_LIMIT - resumesNumber < 3 && (
                      <Pagination
                        sx={{ mb: '1.25em' }}
                        page={page ?? 0}
                        count={Math.ceil(resumesNumber / CARD_LIMIT)}
                        color="secondary"
                        onChange={handlePagination}
                      />
                    )}
                </>
              ) : (
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    mt: '5em',
                  }}
                >
                  <CircularProgress color="secondary" size="2em" />
                  <Typography mt="1em" variant="body2" color="secondary">
                    Получение резюме...
                  </Typography>
                </Box>
              )}
            </Box>
          </Grid>
        </Grid>

        <Snackbar open={!!resumesFetchError} autoHideDuration={3000}>
          <Alert severity="error" sx={{ width: '100%' }}>
            {resumesFetchError}
          </Alert>
        </Snackbar>
      </Container>
    );
  }
}

export default Resumes;
