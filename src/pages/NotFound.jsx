import React from 'react';
import { Typography, Container } from '@mui/material';

function NotFound() {
  return (
    <Container
      maxWidth="md"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Typography mt="5em" component="h1" variant="h4">
        404
      </Typography>
      <Typography component="h1" variant="h5" textAlign="center">
        Данная страница не найдена...
      </Typography>
    </Container>
  );
}

export default NotFound;
