import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Typography, Button, Box } from '@mui/material';
import { blue } from '@mui/material/colors';

function Homepage() {
  return (
    <Container maxWidth="lg">
      <Box display="flex" flexDirection="column" mt="5em">
        <Typography
          variant="h4"
          fontWeight={500}
          color="primary"
          display="flex"
          justifyContent="center"
          textAlign="center"
        >
          Домашний персонал без посредников
        </Typography>
        <Typography
          variant="h6"
          fontWeight={400}
          color="primary"
          display="flex"
          justifyContent="center"
          textAlign="center"
        >
          Совершенно бесплатно
        </Typography>
        <Typography
          mt="2rem"
          fontWeight={300}
          color="primary"
          display="flex"
          justifyContent="center"
          textAlign="center"
        >
          Здесь Вы сможете найти постоянную работу, которая подойдёт именно Вам!
        </Typography>
      </Box>

      <Box display="flex" justifyContent="center" mt="2em">
        <Button
          variant="contained"
          sx={{ margin: '0 0.5rem', textAlign: 'center' }}
        >
          <Link
            to="/vacancies"
            style={{ textDecoration: 'none', color: 'white' }}
          >
            Найти работу
          </Link>
        </Button>
        <Button
          variant="outlined"
          sx={{ margin: '0 0.5rem', textAlign: 'center' }}
        >
          <Link
            to="/resumes"
            style={{ textDecoration: 'none', color: blue[700] }}
          >
            Найти работника
          </Link>
        </Button>
      </Box>
    </Container>
  );
}

export default Homepage;
