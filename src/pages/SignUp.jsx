import React, { useCallback, useState } from 'react';
import {
  Button,
  CssBaseline,
  TextField,
  Grid,
  Box,
  Typography,
  Container,
  Divider,
  ButtonGroup,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Snackbar,
  Alert,
  Autocomplete,
  FormControlLabel,
  Checkbox,
} from '@mui/material';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, Link } from 'react-router-dom';

import debounce from 'lodash.debounce';
import AuthLogo from '../components/AuthLogo';
import registrationValidator from '../helpers/registrationValidator';
import { requestAuthUser } from '../redux/actions/auth';
import { getCities } from '../api/city';

function SignUp() {
  const formik = useFormik({
    initialValues: {
      lastName: '',
      firstName: '',
      patronymic: '',
      city: '',
      phoneNumber: '',
      age: '',
      totalExperience: '',
      password: '',
      repeatedPassword: '',
    },
    validationSchema: registrationValidator,
  });

  const [role, setRole] = useState('workers');
  const [gender, setGender] = useState('');
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [filteredCities, setFilteredCities] = useState([]);
  const [citySearchQuery, setCitySearchQuery] = useState('');
  const [politicsAgreement, setPoliticsAgreement] = useState(false);

  const dispatch = useDispatch();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleCitySearch = useCallback(
    debounce(async (query) => {
      const fetchedData = await getCities({ query });
      setFilteredCities(fetchedData);
      setCitySearchQuery(query);
    }, 500),
    []
  );

  const handleInputChange = (_, value) => {
    if (value.trim() === '' || value.trim().length < 3) {
      setFilteredCities([]);
      setCitySearchQuery('');
      return;
    }

    handleCitySearch(value.trim());
  };

  const handleOnFocusAutocomplete = () => {
    if (filteredCities) setFilteredCities([]);
    if (citySearchQuery) setCitySearchQuery('');
  };

  const handleClickSnackbar = () => {
    setOpenSnackbar(true);
  };

  const handleCloseSnackbar = (_, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };

  const handleSelectRole = (event) => {
    setRole(event.target.id);
  };

  const handleSelectGender = (event) => {
    setGender(event.target.value);
  };

  const handleSubmitForm = (event) => {
    event.preventDefault();
    handleClickSnackbar();

    const payload = {
      ...formik.values,
      gender,
      userGroup: role,
      authAction: 'registration',
    };

    dispatch(requestAuthUser(payload));
  };

  const { authUser, error: registrationRequestError } = useSelector(
    (state) => state.authReducer
  );

  const isAuth = Object.keys(authUser).length;
  const isAuthError = registrationRequestError !== null;
  const isAuthErrorObject = typeof registrationRequestError === 'object';

  if (isAuth) {
    return <Navigate to="/" />;
  }

  const privacy = `${import.meta.env.VITE_FILES_URL}/privacy.pdf`;
  const agreement = `${import.meta.env.VITE_FILES_URL}/agreement.pdf`;

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <Box
        sx={{
          marginTop: '2.5rem',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <AuthLogo title="Регистрация" />

        <ButtonGroup disableElevation variant="outlined">
          <Button
            id="workers"
            variant={role === 'workers' ? 'contained' : 'outlined'}
            onClick={handleSelectRole}
          >
            Работник
          </Button>
          <Button
            id="employers"
            variant={role === 'employers' ? 'contained' : 'outlined'}
            onClick={handleSelectRole}
          >
            Работодатель
          </Button>
        </ButtonGroup>

        <Box
          component="form"
          noValidate
          onSubmit={handleSubmitForm}
          sx={{ mt: 3 }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="lastName"
                label="Фамилия"
                name="lastName"
                autoComplete="family-name"
                helperText={
                  formik.touched.lastName ? formik.errors.lastName : ''
                }
                error={
                  formik.touched.lastName && Boolean(formik.errors.lastName)
                }
                value={formik.values.lastName}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                required
                fullWidth
                id="firstName"
                label="Имя"
                helperText={
                  formik.touched.firstName ? formik.errors.firstName : ''
                }
                error={
                  formik.touched.firstName && Boolean(formik.errors.firstName)
                }
                value={formik.values.firstName}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                id="patronymic"
                label="Отчество"
                name="patronymic"
                autoComplete="patronymic"
                helperText={
                  formik.touched.patronymic ? formik.errors.patronymic : ''
                }
                error={
                  formik.touched.patronymic && Boolean(formik.errors.patronymic)
                }
                value={formik.values.patronymic}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Autocomplete
                freeSolo={Boolean(citySearchQuery.length < 3)}
                id="city"
                noOptionsText="Ничего не найдено"
                options={filteredCities}
                onInputChange={handleInputChange}
                onFocus={handleOnFocusAutocomplete}
                name="city"
                autoComplete="city"
                value={formik.values.city}
                onChange={(_, value) => formik.setFieldValue('city', value)}
                onBlur={formik.handleBlur}
                renderInput={(params) => (
                  <TextField
                    // eslint-disable-next-line react/jsx-props-no-spreading
                    {...params}
                    label="Город проживания"
                    helperText={formik.touched.city ? formik.errors.city : ''}
                    error={formik.touched.city && Boolean(formik.errors.city)}
                  />
                )}
              />
            </Grid>
          </Grid>

          <Divider variant="middle" sx={{ margin: '1.5rem 0' }} />

          <Grid container spacing={2}>
            {role === 'employers' ? (
              <>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="phoneNumber"
                    label="Номер телефона"
                    name="phoneNumber"
                    autoComplete="phone-number"
                    helperText={
                      formik.touched.phoneNumber
                        ? formik.errors.phoneNumber
                        : ''
                    }
                    error={
                      formik.touched.phoneNumber &&
                      Boolean(formik.errors.phoneNumber)
                    }
                    value={formik.values.phoneNumber}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
                <Grid item xs={12} sm={3}>
                  <TextField
                    required
                    fullWidth
                    id="age"
                    label="Возраст"
                    name="age"
                    autoComplete="age"
                    helperText={formik.touched.age ? formik.errors.age : ''}
                    error={formik.touched.age && Boolean(formik.errors.age)}
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
              </>
            ) : (
              <>
                <Grid item xs={12} sm={3}>
                  <TextField
                    required
                    fullWidth
                    id="phoneNumber"
                    label="Номер телефона"
                    name="phoneNumber"
                    autoComplete="phone-number"
                    helperText={
                      formik.touched.phoneNumber
                        ? formik.errors.phoneNumber
                        : ''
                    }
                    error={
                      formik.touched.phoneNumber &&
                      Boolean(formik.errors.phoneNumber)
                    }
                    value={formik.values.phoneNumber}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
                <Grid item xs={12} sm={3}>
                  <TextField
                    required
                    fullWidth
                    id="totalExperience"
                    label="Общий стаж"
                    name="totalExperience"
                    autoComplete="totalExperience"
                    helperText={
                      formik.touched.totalExperience
                        ? formik.errors.totalExperience
                        : ''
                    }
                    error={
                      formik.touched.totalExperience &&
                      Boolean(formik.errors.totalExperience)
                    }
                    value={formik.values.totalExperience}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
                <Grid item xs={12} sm={3}>
                  <TextField
                    required
                    fullWidth
                    id="age"
                    label="Возраст"
                    name="age"
                    autoComplete="age"
                    helperText={formik.touched.age ? formik.errors.age : ''}
                    error={formik.touched.age && Boolean(formik.errors.age)}
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
              </>
            )}

            <Grid item xs={12} sm={3}>
              <FormControl fullWidth>
                <InputLabel id="gender">Пол *</InputLabel>
                <Select
                  required
                  labelId="gender"
                  id="gender"
                  label="Пол *"
                  value={gender}
                  onChange={handleSelectGender}
                >
                  <MenuItem value="М">Мужчина</MenuItem>
                  <MenuItem value="Ж">Женщина</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Divider variant="middle" sx={{ margin: '1.5rem 0' }} />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                name="password"
                label="Пароль"
                type="password"
                id="password"
                autoComplete="new-password"
                helperText={
                  formik.touched.password ? formik.errors.password : ''
                }
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                name="repeatedPassword"
                label="Повтор пароля"
                type="password"
                id="repeatedPassword"
                autoComplete="repeated-password"
                helperText={
                  formik.touched.repeatedPassword
                    ? formik.errors.repeatedPassword
                    : ''
                }
                error={
                  formik.touched.repeatedPassword &&
                  Boolean(formik.errors.repeatedPassword)
                }
                value={formik.values.repeatedPassword}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
          </Grid>

          <Box display="flex" justifyContent="center" mt="0.5em">
            <FormControlLabel
              required
              control={
                <Checkbox
                  size="small"
                  checked={politicsAgreement}
                  onChange={(event) =>
                    setPoliticsAgreement(event.target.checked)
                  }
                />
              }
              label={
                <Typography variant="body2">
                  Соглашаюсь с условиями{' '}
                  <Link to={privacy} target="_blank" variant="body2">
                    политики конфиденциальности
                  </Link>{' '}
                  и{' '}
                  <Link to={agreement} target="_blank" variant="body2">
                    пользовательского соглашения
                  </Link>{' '}
                </Typography>
              }
            />
          </Box>

          <Box display="flex" justifyContent="center">
            <Button
              disabled={
                (Object.keys(formik.errors).length !== 0 &&
                  !(
                    Object.keys(formik.errors).length === 1 &&
                    role === 'employers' &&
                    formik.errors.totalExperience
                  )) ||
                gender === '' ||
                !politicsAgreement
              }
              type="submit"
              variant="contained"
              sx={{ mt: '0.25em', mb: '0.5em' }}
            >
              Зарегистрироваться
            </Button>
          </Box>

          <Grid container justifyContent="center" mb="3em">
            <Grid item display="flex" alignItems="center">
              <Typography variant="body2" sx={{ mr: '0.25rem' }}>
                Уже есть аккаунт?
              </Typography>

              <Link to="/sign-in" variant="body2">
                Войти
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>

      {isAuthError && (
        <Snackbar
          open={isAuthError && openSnackbar}
          autoHideDuration={5000}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
          onClose={handleCloseSnackbar}
        >
          <Alert
            severity="error"
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
            }}
            onClose={handleCloseSnackbar}
          >
            {isAuthErrorObject ? (
              Object.values(registrationRequestError).map((error) => (
                <div key={error}>{error}</div>
              ))
            ) : (
              <div>{registrationRequestError}</div>
            )}
          </Alert>
        </Snackbar>
      )}
    </Container>
  );
}

export default SignUp;
