import React from 'react';
import {
  Box,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

function SelectAccordion({ title, items, value, setValue, color }) {
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Accordion
      disableGutters
      sx={{
        boxShadow: 'none',
        bgcolor: 'primary.grey',
      }}
    >
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Box width="17em" display="flex" justifyContent="space-evenly" px="1em">
          <FormControl fullWidth>
            <InputLabel color={color} id="demo-simple-select-label">
              {title}
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={value}
              label={title}
              onChange={handleChange}
              color={color}
            >
              {items.map((item) => (
                <MenuItem key={item.type || item} value={item.type || item}>
                  {item.text || item}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
      </AccordionDetails>
    </Accordion>
  );
}
export default SelectAccordion;
