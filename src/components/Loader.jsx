import React from 'react';
import { Box, Container, CircularProgress, Typography } from '@mui/material';

function Loader({ text }) {
  return (
    <Container
      sx={{
        display: 'flex',
        mt: '12em',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <CircularProgress size="3em" />
        <Typography mt="1em" variant="body2" color="primary.main">
          {text}
        </Typography>
      </Box>
    </Container>
  );
}

export default Loader;
