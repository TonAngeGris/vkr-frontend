import React from 'react';

import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
} from '@mui/material';

function RoleSelection({ setRole }) {
  const roleHandler = (event) => {
    setRole(event.target.value);
  };

  return (
    <FormControl>
      <FormLabel
        id="demo-row-radio-buttons-group-label"
        sx={{ textAlign: 'center' }}
      >
        Тип аккаунта
      </FormLabel>
      <RadioGroup
        row
        name="use-radio-group"
        defaultValue="first"
        onChange={roleHandler}
      >
        <FormControlLabel value="worker" control={<Radio />} label="Работник" />
        <FormControlLabel
          value="employer"
          control={<Radio />}
          label="Работодатель"
          sx={{ margin: 0 }}
        />
      </RadioGroup>
    </FormControl>
  );
}

export default RoleSelection;
