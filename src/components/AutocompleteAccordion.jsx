import React, { useCallback, useState } from 'react';
import {
  Box,
  TextField,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  Autocomplete,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import debounce from 'lodash.debounce';

function AutocompleteAccordion({
  title,
  description,
  value,
  setValue,
  fetchValues,
  color,
}) {
  const [filteredItems, setFilteredItems] = useState([]);
  const [itemSearchQuery, setItemSearchQuery] = useState('');

  const handleChange = (_, val) => {
    setValue(val);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleItemSearch = useCallback(
    debounce(async (query) => {
      const fetchedData = await fetchValues({ query });
      setFilteredItems(fetchedData);
      setItemSearchQuery(query);
    }, 500),
    []
  );

  const handleInputChange = (_, val) => {
    if (val.trim() === '' || val.trim().length < 3) {
      setFilteredItems([]);
      setItemSearchQuery('');
      return;
    }

    handleItemSearch(val.trim());
  };

  const handleOnFocusAutocomplete = () => {
    if (filteredItems) setFilteredItems([]);
    if (itemSearchQuery) setItemSearchQuery('');
  };

  return (
    <Accordion
      disableGutters
      sx={{
        boxShadow: 'none',
        bgcolor: 'primary.grey',
      }}
    >
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Box width="17em" display="flex" justifyContent="space-evenly" px="1em">
          <Autocomplete
            freeSolo={Boolean(itemSearchQuery.length < 3)}
            noOptionsText="Ничего не найдено"
            isOptionEqualToValue={(option, val) => option.value === val.value}
            options={filteredItems}
            onInputChange={handleInputChange}
            onFocus={handleOnFocusAutocomplete}
            value={value}
            onChange={handleChange}
            color={color}
            renderInput={(params) => (
              <TextField
                color={color}
                sx={{ width: '15em' }}
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...params}
                label={description || title}
              />
            )}
          />
        </Box>
      </AccordionDetails>
    </Accordion>
  );
}
export default AutocompleteAccordion;
