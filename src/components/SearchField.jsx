import React from 'react';
import { Box, TextField } from '@mui/material';
import debounce from 'lodash.debounce';

function SearchField({ label, setValue, color }) {
  const handleChange = debounce((event) => setValue(event.target.value), 200);

  return (
    <Box display="flex" alignItems="center" my="1em">
      <TextField
        fullWidth
        label={label}
        color={color}
        id="search"
        size="small"
        onChange={handleChange}
      />
    </Box>
  );
}

export default SearchField;
