import React, { useState } from 'react';
import {
  Paper,
  Typography,
  Box,
  Divider,
  Avatar,
  Rating,
  Button,
  Chip,
  Tooltip,
  Grid,
  Snackbar,
  Alert,
} from '@mui/material';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import StarIcon from '@mui/icons-material/Star';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';

import { useDispatch, useSelector } from 'react-redux';
import ResumeUpdateDialog from './ResumeUpdateDialog';
import { deleteResume } from '../api/resume';
import { deleteWorkerResume } from '../redux/actions/user';
import { readUserTypeFromLS } from '../lib/localStorage';
import ContractCreateDialog from './ContractCreateDialog';

const daysOfWeek = {
  monday: 'Пн',
  tuesday: 'Вт',
  wednesday: 'Ср',
  thursday: 'Чт',
  friday: 'Пт',
  sunday: 'Сб',
  saturday: 'Вс',
};

function ResumeCard({
  id,
  title,
  description,
  city,
  age,
  experience,
  wage,
  workerFirstName,
  workerLastName,
  createdAt,
  rating,
  role,
  descriptionTextStyles,
  showMore,
  schedule,
  workerId,
  isAccountOwnerAuth,
}) {
  const [editResumeDialogOpened, setEditResumeDialogOpened] = useState(false);
  const [createContractDialogOpened, setCreateContractDialogOpened] =
    useState(false);

  const [updateError, setUpdateError] = useState('');
  const [contractCreateSuccess, setContractCreateSuccess] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();

  const handleEditResumeDialogOpen = () => {
    setEditResumeDialogOpened(true);
  };

  const handleEditResumeDialogClose = () => {
    setEditResumeDialogOpened(false);
  };

  const handleContractCreateDialogOpen = () => {
    setCreateContractDialogOpened(true);
  };

  const handleContractCreateDialogClose = () => {
    setCreateContractDialogOpened(false);
  };

  const handleCloseSnackbar = (_, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setUpdateError('');
  };

  const handleDeleteResume = async (event) => {
    event.preventDefault();

    try {
      await deleteResume(id);
      dispatch(deleteWorkerResume({ id }));
    } catch (error) {
      setUpdateError(error?.response?.data?.message);
    }
  };

  let experienceText = 'год';
  if (experience % 10 !== 1) {
    if (
      [2, 3, 4].includes(experience % 10) &&
      ![10, 11, 12, 13, 14].includes(experience)
    ) {
      experienceText = 'года';
    } else {
      experienceText = 'лет';
    }
  }

  let ageText = 'год';
  if (age % 10 !== 1) {
    if ([2, 3, 4].includes(age % 10) && ![10, 11, 12, 13, 14].includes(age)) {
      ageText = 'года';
    } else {
      ageText = 'лет';
    }
  }

  const formattedWage = wage
    ? Number(wage).toLocaleString('ru-RU', {
        style: 'currency',
        currency: 'RUB',
      })
    : 'Договорная';

  const showMoreHandler = () => {
    navigate(`/resumes/${id}`);
  };

  const { authUser } = useSelector((state) => state.authReducer);

  const createdAtFormatted = dayjs(createdAt).format('DD.MM.YYYY');
  const isResumePage = location.pathname.match(/resumes\/\d+$/);
  const isEmployersGroup = readUserTypeFromLS() === 'employers';
  const isAuth = Boolean(Object.keys(authUser).length);

  return (
    <Paper variant="outlined" sx={{ mb: '0.75em', p: '1em', width: '300%' }}>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Typography variant="h5" sx={{ fontWeight: 700 }}>
          {title}
        </Typography>
        <Chip label={createdAtFormatted} />
      </Box>

      <Box display="flex" color="primary.darkgrey">
        <LocationOnIcon fontSize="small" />
        <Typography mt="0.1em" variant="body2">
          {city}
        </Typography>
      </Box>

      <Box display="flex" my="0.5em">
        {age && (
          <Box mr="2em">
            <Typography variant="body2" color="primary.darkgrey">
              Возраст
            </Typography>
            <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
              {age} {ageText}
            </Typography>
          </Box>
        )}
        <Box mr="2em">
          <Typography variant="body2" color="primary.darkgrey">
            Опыт
          </Typography>
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {experience} {experienceText}
          </Typography>
        </Box>
        <Box mr="2em">
          <Typography variant="body2" color="primary.darkgrey">
            Зарплата
          </Typography>
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {formattedWage}
          </Typography>
        </Box>
        <Box>
          <Typography variant="body2" color="primary.darkgrey">
            Роль
          </Typography>
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {role}
          </Typography>
        </Box>

        {isAccountOwnerAuth && (
          <Box ml="auto" display="flex" alignItems="start">
            <Box display="flex" flexDirection="column">
              <Button
                variant="outlined"
                size="small"
                onClick={handleEditResumeDialogOpen}
                sx={{ mb: '0.5em' }}
              >
                Изменить
              </Button>
              <Button
                variant="outlined"
                size="small"
                onClick={handleDeleteResume}
                sx={{ color: '#d81b60', borderColor: '#d81b60' }}
              >
                Удалить
              </Button>
            </Box>
          </Box>
        )}

        {isEmployersGroup && isAuth && isResumePage && (
          <Box
            display="flex"
            ml="auto"
            justifyContent="end"
            alignItems="center"
          >
            <Button
              variant="outlined"
              size="small"
              color="secondary"
              onClick={handleContractCreateDialogOpen}
            >
              Договор
            </Button>
          </Box>
        )}
      </Box>

      <Typography
        variant="body1"
        mb="0.5em"
        sx={{
          ...descriptionTextStyles,
          hyphens: 'auto',
          wordBreak: 'break-word',
        }}
      >
        {description}
      </Typography>

      <Divider />

      <Box mt="0.5em" display="flex" alignItems="center">
        {workerFirstName && (
          <Link to={`/workers/${workerId}`} style={{ textDecoration: 'none' }}>
            <Box display="flex" alignItems="center">
              <Avatar sx={{ bgcolor: 'secondary.main', fontSize: '20px' }}>
                {workerFirstName[0]}
                {workerLastName[0]}
              </Avatar>
              <Typography
                mx="0.5em"
                variant="body2"
                color="black"
                sx={{ fontWeight: 500 }}
              >
                {workerFirstName} {workerLastName}
              </Typography>
            </Box>
          </Link>
        )}

        {rating && (
          <Rating
            size="small"
            precision={0.25}
            value={Number(rating)}
            emptyIcon={
              <StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />
            }
            readOnly
          />
        )}
        <Tooltip
          title={
            schedule &&
            Object.keys(schedule).filter((item) => schedule[item]).length ? (
              <Grid container width="11em">
                <Grid item xs={3}>
                  {schedule &&
                    Object.keys(schedule).map((key) => (
                      <Typography key={key} variant="body1">
                        {daysOfWeek[key]}:
                      </Typography>
                    ))}
                </Grid>
                <Grid item xs={9}>
                  {schedule &&
                    Object.keys(schedule).map((key) => (
                      <Typography key={key} variant="body1">
                        {schedule[key]?.start_time
                          ? `${schedule[key]?.start_time.slice(
                              0,
                              5
                            )}-${schedule[key]?.end_time.slice(0, 5)}`
                          : 'выходной'}
                      </Typography>
                    ))}
                </Grid>
              </Grid>
            ) : (
              ''
            )
          }
          componentsProps={{
            tooltip: {
              sx: {
                bgcolor: '#EBEBEB',
                borderRadius: '10px',
                color: 'black',
                p: '1em 1.5em',
              },
            },
          }}
        >
          <Chip
            sx={{ bgcolor: '#EBEBEB', ml: '0.5em' }}
            label={
              <Typography variant="body1" sx={{ cursor: 'pointer' }}>
                График:{' '}
                {schedule &&
                Object.keys(schedule).filter((item) => schedule[item]).length
                  ? Object.keys(schedule).map(
                      (key, idx) =>
                        schedule[key]?.start_time &&
                        `${idx !== 0 ? ', ' : ''}${daysOfWeek[key]}`
                    )
                  : 'договорной'}
              </Typography>
            }
          />
        </Tooltip>

        {!showMore && (
          <Button
            variant="outlined"
            size="small"
            color="secondary"
            sx={{ ml: 'auto' }}
            onClick={showMoreHandler}
          >
            Подробнее
          </Button>
        )}
      </Box>

      <Snackbar
        open={Boolean(updateError)}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        onClose={handleCloseSnackbar}
      >
        <Alert
          severity="error"
          sx={{ width: '100%' }}
          onClose={handleCloseSnackbar}
        >
          {updateError}
        </Alert>
      </Snackbar>

      <Snackbar
        open={Boolean(contractCreateSuccess)}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        onClose={handleCloseSnackbar}
      >
        <Alert
          severity="success"
          sx={{ width: '100%' }}
          onClose={handleCloseSnackbar}
        >
          Предложение заключить договор отправлено работнику!
        </Alert>
      </Snackbar>

      <ResumeUpdateDialog
        open={editResumeDialogOpened}
        handleClose={handleEditResumeDialogClose}
        color="secondary"
        setError={setUpdateError}
        workerId={workerId}
        resumeData={{
          id,
          title,
          description,
          city,
          experience,
          wage,
          role,
          schedule,
        }}
      />

      <ContractCreateDialog
        open={createContractDialogOpened}
        handleClose={handleContractCreateDialogClose}
        color="secondary"
        setError={setUpdateError}
        setSuccess={setContractCreateSuccess}
        contractData={{
          title,
          city,
          wage,
          role,
          workerId,
          authUserId: authUser.id,
        }}
      />
    </Paper>
  );
}

export default ResumeCard;
