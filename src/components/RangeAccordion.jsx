import React from 'react';
import {
  Box,
  TextField,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

function RangeAccordion({ title, value, setValue, color }) {
  const handleChangeMinValue = (event) => {
    setValue({ ...value, min: event.target.value });
  };

  const handleChangeMaxValue = (event) => {
    setValue({ ...value, max: event.target.value });
  };

  return (
    <Accordion
      disableGutters
      sx={{
        boxShadow: 'none',
        bgcolor: 'primary.grey',
      }}
    >
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Box width="17em" display="flex" justifyContent="space-evenly" px="1em">
          <Box width="40%">
            <TextField
              id="min-value"
              label="От"
              value={value.min}
              color={color}
              onChange={handleChangeMinValue}
            />
          </Box>
          <Box width="40%">
            <TextField
              id="max-value"
              label="До"
              value={value.max}
              color={color}
              onChange={handleChangeMaxValue}
            />
          </Box>
        </Box>
      </AccordionDetails>
    </Accordion>
  );
}
export default RangeAccordion;
