import React from 'react';
import { Outlet } from 'react-router-dom';
import { Box } from '@mui/material';

import Footer from './Footer';
import Navbar from './Navbar';

function Layout() {
  return (
    <Box display="flex" flexDirection="column" minHeight="100vh">
      <Navbar />
      <Box flexGrow={1}>
        <Outlet />
      </Box>
      <Footer />
    </Box>
  );
}

export default Layout;
