import React from 'react';
import { Chip, Grid, Tooltip, Typography } from '@mui/material';

const additionalInfoShortcuts = {
  smoking: 'Курение',
  foreign_passport: 'Загран паспорт',
  ready_for_moving: 'Переезд',
  ready_for_traveling: 'Командировки',
  have_children: 'Дети',
  driver_licence: 'Водительские права',
  nonconviction_certificate: 'Несудимость',
  medicine_certificate: 'Мед. справки',
  mental_state_certificate: 'Справка из ПНД',
  medical_tests: 'Мед. тесты',
  can_swimming: 'Умение плавать',
};

function AdditionalInfoChip({ additionalInfo, title }) {
  return (
    <Tooltip
      title={
        additionalInfo &&
        Object.keys(additionalInfo).filter(
          (key) => key !== 'id' && additionalInfo[key] !== null
        ).length ? (
          <Grid container width="19em">
            <Grid item xs={10}>
              {additionalInfo &&
                Object.keys(additionalInfo)
                  .filter((key) => key !== 'id' && additionalInfo[key] !== null)
                  .map((key) => (
                    <Typography key={key} variant="body1">
                      {additionalInfoShortcuts[key]}:
                    </Typography>
                  ))}
            </Grid>
            <Grid item xs={2}>
              {additionalInfo &&
                Object.keys(additionalInfo)
                  .filter((key) => key !== 'id' && additionalInfo[key] !== null)
                  .map((key) => {
                    return (
                      <Typography key={key} variant="body1" textAlign="center">
                        {additionalInfo[key] ? 'Да' : 'Нет'}
                      </Typography>
                    );
                  })}
            </Grid>
          </Grid>
        ) : (
          <Typography variant="body1" textAlign="center">
            Отсутствуют
          </Typography>
        )
      }
      componentsProps={{
        tooltip: {
          sx: {
            bgcolor: '#EBEBEB',
            borderRadius: '10px',
            color: 'black',
            p: '1em 1.5em',
          },
        },
      }}
    >
      <Chip
        sx={{
          bgcolor: '#EBEBEB',
          height: 'auto',
          '& .MuiChip-label': {
            display: 'block',
            whiteSpace: 'normal',
          },
        }}
        label={
          <Typography variant="body1" sx={{ cursor: 'pointer', p: '0.05em' }}>
            {title}
          </Typography>
        }
      />
    </Tooltip>
  );
}

export default AdditionalInfoChip;
