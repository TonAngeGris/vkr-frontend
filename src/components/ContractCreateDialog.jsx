import React, { useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
  TextField,
  Button,
  Autocomplete,
} from '@mui/material';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';

import { getRoles } from '../api/role';
import createContractValidator from '../helpers/createContractValidator';
import { createContract } from '../api/contract';

function ContractCreateDialog({
  open,
  handleClose,
  color,
  contractData,
  setError,
  setSuccess,
}) {
  const [filteredRoles, setFilteredRoles] = useState([]);

  const formik = useFormik({
    initialValues: {
      title: contractData?.title || '',
      city: contractData?.city || '',
      wage: contractData?.wage || '',
      role: contractData?.role || '',
    },
    validationSchema: createContractValidator,
  });

  const debouncedOnChange = debounce((field, value) => {
    formik.setFieldValue(field, value);
  }, 200);

  const handleChange = (event) => {
    const { name, value } = event.target;
    debouncedOnChange(name, value);
  };

  const handleDialogClose = () => {
    handleClose();
    // Убираем ошибки и введенные данные при следующем открытии
    formik.setTouched({}, false);
    formik.setValues(formik.initialValues);
  };

  const handleRoleSearch = debounce(async (query) => {
    const fetchedData = await getRoles({ query });
    setFilteredRoles(fetchedData);
  }, 200);

  const handleRoleInputChange = (_, value) => {
    handleRoleSearch(value);
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    const bodyData = {
      ...formik.values,
      workerId: contractData?.workerId || contractData?.authUserId,
      employerId: contractData?.employerId || contractData?.authUserId,
      isWorker: !contractData?.workerId || null,
      isEmployer: !contractData?.employerId || null,
    };

    try {
      await createContract(bodyData);
      setSuccess(true);
    } catch (error) {
      setError(error?.response?.data?.message);
      formik.setValues(formik.initialValues);
    }

    handleClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Создание договора</DialogTitle>

      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Чтобы создать договор просто введите необходимые и нажмите кнопку
          &apos;&apos;Создать&apos;&apos;. После этого ожидайте подтверждения со
          стороны {contractData?.employerId ? 'работодателя' : 'работника'}.
        </DialogContentText>

        <Box
          component="form"
          noValidate
          onSubmit={handleSubmitForm}
          sx={{ mt: 3 }}
        >
          <TextField
            fullWidth
            id="title"
            color={color}
            label="Название"
            name="title"
            autoComplete="title"
            value={contractData?.title || ''}
            sx={{ mb: '1em' }}
          />

          <TextField
            fullWidth
            id="city"
            color={color}
            label="Город"
            name="city"
            autoComplete="city"
            value={contractData?.city || ''}
            sx={{ mb: '1em' }}
          />

          <TextField
            fullWidth
            id="wage"
            color={color}
            label="Зарплата, ₽"
            name="wage"
            autoComplete="wage"
            defaultValue={formik.values.wage}
            sx={{ mb: '1em' }}
            helperText={formik.touched.wage ? formik.errors.wage : ''}
            error={formik.touched.wage && Boolean(formik.errors.wage)}
            onChange={handleChange}
            onBlur={formik.handleBlur}
          />

          <Autocomplete
            id="role"
            color={color}
            noOptionsText="Ничего не найдено"
            options={filteredRoles}
            onInputChange={handleRoleInputChange}
            name="role"
            autoComplete
            defaultValue={formik.values.role}
            onChange={(_, value) => formik.setFieldValue('role', value)}
            onBlur={formik.handleBlur}
            renderInput={(params) => (
              <TextField
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...params}
                label="Специальность"
                color={color}
                helperText={formik.touched.role ? formik.errors.role : ''}
                error={formik.touched.role && Boolean(formik.errors.role)}
              />
            )}
          />

          <Box mt="1em" display="flex" justifyContent="end">
            <Button
              variant="outlined"
              color={color}
              onClick={handleDialogClose}
            >
              Отмена
            </Button>
            <Button
              sx={{ ml: '0.5em' }}
              variant="outlined"
              color={color}
              type="submit"
              disabled={Object.keys(formik.errors).length !== 0}
            >
              Создать
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

export default ContractCreateDialog;
