import React from 'react';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
  TextField,
  Button,
  Rating,
  Typography,
} from '@mui/material';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';
import { useDispatch } from 'react-redux';

import completeContractValidator from '../helpers/completeContractValidator';
import {
  deleteEmployerInProgressContract,
  deleteEmployerPendingCompletionContract,
  deleteWorkerInProgressContract,
  deleteWorkerPendingCompletionContract,
  setEmployerCompletedContract,
  setEmployerPendingCompletionContract,
  setEmployerTotalRating,
  setWorkerCompletedContract,
  setWorkerPendingCompletionContract,
  setWorkerTotalRating,
} from '../redux/actions/user';
import { updateContract } from '../api/contract';

function ContractCompleteDialog({
  open,
  handleClose,
  color,
  contractData,
  isWorker,
  isEmployer,
  setError,
}) {
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      comment: '',
      rating: '',
    },
    validationSchema: completeContractValidator,
  });

  const debouncedOnChange = debounce((field, value) => {
    formik.setFieldValue(field, value);
  }, 200);

  const handleChange = (event) => {
    const { name, value } = event.target;
    debouncedOnChange(name, value);
  };

  const handleDialogClose = () => {
    handleClose();
    // Убираем ошибки и введенные данные при следующем открытии
    formik.setTouched({}, false);
    formik.setValues(formik.initialValues);
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    let bodyData = {
      ...formik.values,
      workerId: contractData?.workerId,
      employerId: contractData?.employerId,
      // workerCompletionAgreement: contractData?.workerCompletionAgreement,
      // employerCompletionAgreement: contractData?.employerCompletionAgreement,
      isWorker: isWorker || false,
      isEmployer: isEmployer || false,
    };

    if (isEmployer) {
      bodyData = {
        ...bodyData,
        workerCompletionAgreement: contractData?.workerCompletionAgreement,
        employerCompletionAgreement: true,
      };
    } else if (isWorker) {
      bodyData = {
        ...bodyData,
        workerCompletionAgreement: true,
        employerCompletionAgreement: contractData?.employerCompletionAgreement,
      };
    }
    try {
      const data = await updateContract(bodyData, contractData.id);

      // Если работник завершает контракт, но работодатель не подтвердил его
      // то отправляется в "ожидаемые завершения" и ждет завершения работодателя
      if (
        isWorker &&
        data.worker_completion_agreement &&
        !data.employer_completion_agreement
      ) {
        dispatch(deleteWorkerInProgressContract({ id: contractData.id }));
        dispatch(
          setWorkerPendingCompletionContract({ completedContract: data })
        );
      }
      // Если работодатель завершает контракт, но работник не подтвердил его
      // то отправляется в "ожидаемые завершения" и ждет завершения работника
      else if (
        isEmployer &&
        !data.worker_completion_agreement &&
        data.employer_completion_agreement
      ) {
        dispatch(deleteEmployerInProgressContract({ id: contractData.id }));
        dispatch(
          setEmployerPendingCompletionContract({ completedContract: data })
        );
      }
      // Если работник завершил контракт, и он был завершён также и у работодателя
      // то отправляется в "завершённые работы"
      else if (
        isWorker &&
        data.worker_completion_agreement &&
        data.employer_completion_agreement
      ) {
        dispatch(setWorkerTotalRating({ rating: data?.worker?.rating }));

        dispatch(
          deleteWorkerPendingCompletionContract({ id: contractData.id })
        );
        dispatch(setWorkerCompletedContract({ completedContract: data }));
      }
      // Если работодатель завершил контракт, и он был завершён также и у работника
      // то отправляется в "завершённые работы"
      else if (
        isEmployer &&
        data.worker_completion_agreement &&
        data.employer_completion_agreement
      ) {
        dispatch(setEmployerTotalRating({ rating: data?.employer?.rating }));

        dispatch(
          deleteEmployerPendingCompletionContract({ id: contractData.id })
        );
        dispatch(setEmployerCompletedContract({ completedContract: data }));
      }
    } catch (error) {
      setError(error?.response?.data?.message);
      formik.setValues(formik.initialValues);
    }

    handleClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Завершение договора</DialogTitle>

      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Перед завершением договора оставьте комментарий и оценку
          {isWorker ? ' работодателю' : ' работнику'}, после чего нажмите кнопку
          &apos;&apos;Завершить&apos;&apos;.
        </DialogContentText>

        <Box
          component="form"
          noValidate
          onSubmit={handleSubmitForm}
          sx={{ mt: 3 }}
        >
          <TextField
            fullWidth
            multiline
            id="comment"
            label="Комментарий"
            name="comment"
            autoComplete="comment"
            color={color}
            helperText={formik.touched.comment ? formik.errors.comment : ''}
            error={formik.touched.comment && Boolean(formik.errors.comment)}
            onChange={handleChange}
            onBlur={formik.handleBlur}
            sx={{ mb: '1em' }}
          />

          <Typography component="legend">Оценка</Typography>
          <Rating
            name="rating"
            size="large"
            precision={0.5}
            value={Number(formik.values.rating)}
            onChange={(_, newValue) => {
              formik.setFieldValue('rating', newValue);
            }}
          />

          <Box mt="1em" display="flex" justifyContent="end">
            <Button
              variant="outlined"
              color={color}
              onClick={handleDialogClose}
            >
              Отмена
            </Button>
            <Button
              sx={{ ml: '0.5em' }}
              variant="outlined"
              color={color}
              type="submit"
              disabled={
                Object.keys(formik.errors).length !== 0 ||
                !formik.values.comment.length
              }
            >
              Завершить
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

export default ContractCompleteDialog;
