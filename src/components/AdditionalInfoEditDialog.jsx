import React, { useCallback, useEffect, useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Box,
  Button,
  ButtonGroup,
  Typography,
  Autocomplete,
  TextField,
} from '@mui/material';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import additionalInfoValidator from '../helpers/additionalInfoValidator';
import { updateWorker } from '../api/worker';
import { setUserMainData } from '../redux/actions/user';
import { setAuthEditedData } from '../redux/actions/auth';
import { getEducations } from '../api/education';
import { getLanguages } from '../api/language';
import { getCitizenships } from '../api/citizenship';

const additionalInfoProperties = {
  foreignPassport: 'Наличие загран паспорта',
  readyForMoving: 'Готовность к переезду',
  readyForTraveling: 'Готовность к командировкам',
  haveChildren: 'Наличие детей',
  driverLicence: 'Наличие водительского удостоверения',
  nonconvictionCertificate: 'Наличие справки о несудимости',
  medicineCertificate: 'Наличие медицинских справок',
  mentalStateCertificate: 'Наличие справки из психдиспансера',
  medicalTests: 'Наличие медицинских тестов',
  smoking: 'Курю',
  canSwimming: 'Умею плавать',
};

function AdditionalInfoEditDialog({
  open,
  handleClose,
  color,
  userAdditionalInfo,
  setError,
}) {
  const [filteredEducations, setFilteredEducations] = useState([]);
  const fixedEducations = userAdditionalInfo?.educations.map(
    (item) => item.education_branch
  );
  const [education, setEducation] = useState(fixedEducations || []);

  const [filteredLanguages, setFilteredLanguages] = useState([]);
  const fixedLanguages = userAdditionalInfo?.languages.map(
    (item) => item.language_name
  );
  const [language, setLanguage] = useState(fixedLanguages || []);

  const [filteredCitizenships, setFilteredCitizenships] = useState([]);
  const fixedCitizenships = userAdditionalInfo?.citizenships.map(
    (item) => item.citizenship_name
  );
  const [citizenship, setCitizenship] = useState(fixedCitizenships || []);

  const formik = useFormik({
    initialValues: {
      smoking: userAdditionalInfo?.smoking || false,
      foreignPassport: userAdditionalInfo?.foreig_passport || false,
      readyForMoving: userAdditionalInfo?.ready_for_moving || false,
      readyForTraveling: userAdditionalInfo?.ready_for_traveling || false,
      haveChildren: userAdditionalInfo?.have_children || false,
      driverLicence: userAdditionalInfo?.driver_licence || false,
      nonconvictionCertificate:
        userAdditionalInfo?.nonconviction_certificate || false,
      medicineCertificate: userAdditionalInfo?.medicine_certificate || false,
      mentalStateCertificate:
        userAdditionalInfo?.mental_state_certificate || false,
      medicalTests: userAdditionalInfo?.medical_tests || false,
      canSwimming: userAdditionalInfo?.can_swimming || false,
    },
    validationSchema: additionalInfoValidator,
  });

  const dispatch = useDispatch();
  const urlParams = useParams();

  const debouncedOnChange = debounce((field, value) => {
    formik.setFieldValue(field, value);
  }, 200);

  const handleDialogClose = () => {
    handleClose();
    // Убираем ошибки и введенные данные при следующем открытии
    formik.setTouched({}, false);
    formik.setValues(formik.initialValues);
    setEducation(fixedEducations || []);
    setLanguage(fixedLanguages || []);
    setCitizenship(fixedCitizenships || []);
  };

  // TODO: попробовать поменять на получение при триггере на открытие
  useEffect(() => {
    async function fetchAdditionalInfoData() {
      const fetchedEducations = await getEducations();
      const fetchedLanguages = await getLanguages();
      const fetchedCitizenships = await getCitizenships();

      setFilteredEducations(fetchedEducations);
      setFilteredLanguages(fetchedLanguages);
      setFilteredCitizenships(fetchedCitizenships);
    }

    fetchAdditionalInfoData();
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleEducationSearch = useCallback(
    debounce(async (query) => {
      const fetchedData = await getEducations({ query });
      setFilteredEducations(fetchedData);
    }, 200),
    []
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleLanguageSearch = useCallback(
    debounce(async (query) => {
      const fetchedData = await getLanguages({ query });
      setFilteredLanguages(fetchedData);
    }, 200),
    []
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleCitizenshipSearch = useCallback(
    debounce(async (query) => {
      const fetchedData = await getCitizenships({ query });
      setFilteredCitizenships(fetchedData);
    }, 200),
    []
  );

  const handleEducationInputChange = (_, value) => {
    handleEducationSearch(value);
  };

  const handleLanguageInputChange = (_, value) => {
    handleLanguageSearch(value);
  };

  const handleCitizenshipInputChange = (_, value) => {
    handleCitizenshipSearch(value);
  };

  const handleEducationOnChange = (_, newValue) => {
    setEducation(newValue);
  };

  const handleLanguageOnChange = (_, newValue) => {
    setLanguage(newValue);
  };

  const handleCitizenshipOnChange = (_, newValue) => {
    setCitizenship(newValue);
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    const bodyData = {
      additionalInfo: {
        ...formik.values,
        educations: education,
        languages: language,
        citizenships: citizenship,
      },
    };

    try {
      const data = await updateWorker(urlParams.id, bodyData);
      dispatch(setUserMainData({ userData: data, userGroup: 'worker' }));
      dispatch(setAuthEditedData(data));
    } catch (error) {
      setError(error?.response?.data?.message);
      formik.setValues(formik.initialValues);
      setEducation(fixedEducations);
      setLanguage(fixedLanguages);
      setCitizenship(fixedCitizenships);
    }

    handleClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Изменение подробной информации
      </DialogTitle>

      <DialogContent>
        <Box
          component="form"
          noValidate
          onSubmit={handleSubmitForm}
          sx={{ mt: 3 }}
        >
          <Autocomplete
            style={{ marginBottom: '0.5em' }}
            multiple
            id="educations"
            noOptionsText="Ничего не найдено"
            options={filteredEducations}
            onInputChange={handleEducationInputChange}
            onChange={handleEducationOnChange}
            value={education}
            renderInput={(params) => (
              <TextField
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...params}
                color={color}
                size="small"
                label="Тип образования"
              />
            )}
          />

          <Autocomplete
            style={{ marginBottom: '0.5em' }}
            multiple
            id="languages"
            noOptionsText="Ничего не найдено"
            options={filteredLanguages}
            onInputChange={handleLanguageInputChange}
            onChange={handleLanguageOnChange}
            value={language}
            renderInput={(params) => (
              <TextField
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...params}
                color={color}
                size="small"
                label="Иностранный язык"
              />
            )}
          />

          <Autocomplete
            style={{ marginBottom: '0.5em' }}
            multiple
            id="citizenships"
            noOptionsText="Ничего не найдено"
            options={filteredCitizenships}
            onInputChange={handleCitizenshipInputChange}
            onChange={handleCitizenshipOnChange}
            value={citizenship}
            renderInput={(params) => (
              <TextField
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...params}
                color={color}
                size="small"
                label="Иностранное гражданство"
              />
            )}
          />

          {Object.keys(formik.values).map((item) => (
            <Box
              key={item}
              mb="0.5em"
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography variant="body1" mr="1em">
                {additionalInfoProperties[item]}
              </Typography>
              <ButtonGroup
                disableElevation
                variant="outlined"
                color="secondary"
              >
                <Button
                  size="small"
                  variant={formik.values[item] ? 'contained' : 'outlined'}
                  onClick={() => debouncedOnChange([item], true)}
                >
                  Да
                </Button>
                <Button
                  size="small"
                  variant={!formik.values[item] ? 'contained' : 'outlined'}
                  onClick={() => debouncedOnChange([item], false)}
                >
                  Нет
                </Button>
              </ButtonGroup>
            </Box>
          ))}

          <Box mt="1em" display="flex" justifyContent="end">
            <Button
              variant="outlined"
              color={color}
              onClick={handleDialogClose}
            >
              Отмена
            </Button>
            <Button
              sx={{ ml: '0.5em' }}
              variant="outlined"
              color={color}
              type="submit"
              disabled={Object.keys(formik.errors).length !== 0}
            >
              Изменить
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

export default AdditionalInfoEditDialog;
