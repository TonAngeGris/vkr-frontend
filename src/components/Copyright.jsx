import React from 'react';
import { Link } from 'react-router-dom';
import { Typography } from '@mui/material';

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Все права защищены © '}
      <Link
        color="inherit"
        to="/"
        style={{ textDecoration: 'none', color: 'black' }}
      >
        Дом.Услуги
      </Link>
      {' 2022-2023'}
    </Typography>
  );
}

export default Copyright;
