import React from 'react';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
  TextField,
  Button,
} from '@mui/material';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import profileDescriptionValidator from '../helpers/profileDescriptionValidator';
import { updateWorker } from '../api/worker';
import { setAuthEditedData } from '../redux/actions/auth';
import { setUserMainData } from '../redux/actions/user';
import { updateEmployer } from '../api/employer';

function ProfileDescriptionEditDialog({
  open,
  handleClose,
  color,
  userDescription,
  setError,
  userGroup,
}) {
  const dispatch = useDispatch();
  const urlParams = useParams();

  const formik = useFormik({
    initialValues: {
      description: userDescription || '',
    },
    validationSchema: profileDescriptionValidator,
  });

  const debouncedOnChange = debounce((field, value) => {
    formik.setFieldValue(field, value);
  }, 200);

  const handleChange = (event) => {
    const { name, value } = event.target;
    debouncedOnChange(name, value);
  };

  const handleDialogClose = () => {
    handleClose();
    // Убираем ошибки и введенные данные при следующем открытии
    formik.setTouched({}, false);
    formik.setValues(formik.initialValues);
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    const bodyData = {
      description: formik.values.description,
    };

    try {
      let data;
      if (userGroup === 'worker') {
        data = await updateWorker(urlParams.id, bodyData);
      } else if (userGroup === 'employer') {
        data = await updateEmployer(urlParams.id, bodyData);
      }
      dispatch(setUserMainData({ userData: data, userGroup }));
      dispatch(setAuthEditedData(data));
    } catch (error) {
      setError(error?.response?.data?.message);
      formik.setValues(formik.initialValues);
    }

    handleClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Изменение информации о себе
      </DialogTitle>

      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Чтобы изменить информацию о себе просто введите необходимые и нажмите
          кнопку &apos;&apos;Изменить&apos;&apos;
        </DialogContentText>

        <Box
          component="form"
          noValidate
          onSubmit={handleSubmitForm}
          sx={{ mt: 3 }}
        >
          <TextField
            fullWidth
            multiline
            color={color}
            id="description"
            label="О себе"
            name="description"
            autoComplete="description"
            defaultValue={formik.values.description}
            helperText={
              formik.touched.description ? formik.errors.description : ''
            }
            error={
              formik.touched.description && Boolean(formik.errors.description)
            }
            // value={formik.values.description}
            onChange={handleChange}
            onBlur={formik.handleBlur}
          />

          <Box mt="1em" display="flex" justifyContent="end">
            <Button
              variant="outlined"
              color={color}
              onClick={handleDialogClose}
            >
              Отмена
            </Button>
            <Button
              sx={{ ml: '0.5em' }}
              variant="outlined"
              color={color}
              type="submit"
              disabled={Object.keys(formik.errors).length !== 0}
            >
              Изменить
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

export default ProfileDescriptionEditDialog;
