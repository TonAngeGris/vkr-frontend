import {
  Autocomplete,
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { getCities } from '../api/city';
import { updateEmployer } from '../api/employer';
import { updateWorker } from '../api/worker';
import editWorkerProfileValidator from '../helpers/editWorkerProfileValidator';
import editEmployerProfileValidator from '../helpers/editEmployerProfileValidator';
import { setAuthEditedData } from '../redux/actions/auth';
import { setUserMainData } from '../redux/actions/user';

function ProfileEditDialog({
  open,
  handleClose,
  color,
  userData,
  setError,
  userGroup,
}) {
  const [filteredCities, setFilteredCities] = useState([]);
  const [citySearchQuery, setCitySearchQuery] = useState('');
  const [gender, setGender] = useState(userData.gender);

  const dispatch = useDispatch();
  const urlParams = useParams();

  let formikInitValues = {
    lastName: userData.lastname || '',
    firstName: userData.firstname || '',
    patronymic: userData.patronymic || '',
    city: userData?.city?.city_name || '',
    phoneNumber: userData.phone_number || '',
    age: userData.age || '',
    totalExperience: userData.total_experience || '',
    password: '',
    repeatedPassword: '',
  };

  if (userGroup === 'worker') {
    formikInitValues = {
      ...formikInitValues,
      totalExperience: userData.total_experience || '',
    };
  }

  let formikValidationSchema;
  if (userGroup === 'worker') {
    formikValidationSchema = editWorkerProfileValidator;
  } else if (userGroup === 'employer') {
    formikValidationSchema = editEmployerProfileValidator;
  }

  const formik = useFormik({
    initialValues: formikInitValues,
    validationSchema: formikValidationSchema,
  });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleCitySearch = useCallback(
    debounce(async (query) => {
      const fetchedData = await getCities({ query });
      setFilteredCities(fetchedData);
      setCitySearchQuery(query);
    }, 500),
    []
  );

  const handleInputChange = (_, value) => {
    if (value.trim() === '' || value.trim().length < 3) {
      setFilteredCities([]);
      setCitySearchQuery('');
      return;
    }

    if (value.trim() !== userData.city.city_name) {
      handleCitySearch(value.trim());
    }
  };

  const handleOnFocusAutocomplete = () => {
    if (filteredCities) setFilteredCities([]);
    if (citySearchQuery) setCitySearchQuery('');
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    try {
      const { values } = formik;

      const bodyData = {
        gender,
        ...values,
      };

      let data;
      if (userGroup === 'worker') {
        data = await updateWorker(urlParams.id, bodyData);
      } else if (userGroup === 'employer') {
        data = await updateEmployer(urlParams.id, bodyData);
      }

      dispatch(setUserMainData({ userData: data, userGroup }));
      dispatch(setAuthEditedData(data));
    } catch (error) {
      setError(error?.response?.data?.message);
      formik.setValues(formik.initialValues);
    }

    handleClose();
  };

  const handleDialogClose = () => {
    handleClose();
    // Убираем ошибки и введенные данные при следующем открытии
    formik.setTouched({}, false);
    formik.setValues(formik.initialValues);
  };

  const handleSelectGender = (event) => {
    setGender(event.target.value);
  };

  return (
    <Dialog
      open={open}
      onClose={handleDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        Изменение основной информации
      </DialogTitle>

      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Чтобы изменить основную информацию профиля просто введите необходимые
          данные без ошибок и нажмите кнопку &apos;&apos;Изменить&apos;&apos;
        </DialogContentText>

        <Box
          component="form"
          noValidate
          onSubmit={handleSubmitForm}
          sx={{ mt: 3 }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="lastName"
                label="Фамилия"
                name="lastName"
                autoComplete="family-name"
                helperText={
                  formik.touched.lastName ? formik.errors.lastName : ''
                }
                error={
                  formik.touched.lastName && Boolean(formik.errors.lastName)
                }
                value={formik.values.lastName}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                required
                fullWidth
                id="firstName"
                label="Имя"
                helperText={
                  formik.touched.firstName ? formik.errors.firstName : ''
                }
                error={
                  formik.touched.firstName && Boolean(formik.errors.firstName)
                }
                value={formik.values.firstName}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                id="patronymic"
                label="Отчество"
                name="patronymic"
                autoComplete="patronymic"
                helperText={
                  formik.touched.patronymic ? formik.errors.patronymic : ''
                }
                error={
                  formik.touched.patronymic && Boolean(formik.errors.patronymic)
                }
                value={formik.values.patronymic}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="phoneNumber"
                label="Номер телефона"
                name="phoneNumber"
                autoComplete="phone-number"
                helperText={
                  formik.touched.phoneNumber ? formik.errors.phoneNumber : ''
                }
                error={
                  formik.touched.phoneNumber &&
                  Boolean(formik.errors.phoneNumber)
                }
                value={formik.values.phoneNumber}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
          </Grid>

          <Divider variant="middle" sx={{ margin: '1.5rem 0' }} />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <Autocomplete
                freeSolo={Boolean(citySearchQuery.length < 3)}
                id="city"
                noOptionsText="Ничего не найдено"
                options={filteredCities}
                onInputChange={handleInputChange}
                onFocus={handleOnFocusAutocomplete}
                name="city"
                autoComplete
                value={formik.values.city}
                onChange={(_, value) => formik.setFieldValue('city', value)}
                onBlur={formik.handleBlur}
                renderInput={(params) => (
                  <TextField
                    // eslint-disable-next-line react/jsx-props-no-spreading
                    {...params}
                    label="Город проживания"
                    helperText={formik.touched.city ? formik.errors.city : ''}
                    error={formik.touched.city && Boolean(formik.errors.city)}
                  />
                )}
              />
            </Grid>

            {userGroup === 'employer' ? (
              <>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="age"
                    label="Возраст"
                    name="age"
                    autoComplete="age"
                    helperText={formik.touched.age ? formik.errors.age : ''}
                    error={formik.touched.age && Boolean(formik.errors.age)}
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <FormControl fullWidth>
                    <InputLabel id="gender">Пол *</InputLabel>
                    <Select
                      required
                      labelId="gender"
                      id="gender"
                      label="Пол *"
                      value={gender}
                      onChange={handleSelectGender}
                    >
                      <MenuItem value="М">Мужчина</MenuItem>
                      <MenuItem value="Ж">Женщина</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </>
            ) : (
              <>
                <Grid item xs={12} sm={4}>
                  <TextField
                    required
                    fullWidth
                    id="totalExperience"
                    label="Общий стаж"
                    name="totalExperience"
                    autoComplete="totalExperience"
                    helperText={
                      formik.touched.totalExperience
                        ? formik.errors.totalExperience
                        : ''
                    }
                    error={
                      formik.touched.totalExperience &&
                      Boolean(formik.errors.totalExperience)
                    }
                    value={formik.values.totalExperience}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <TextField
                    required
                    fullWidth
                    id="age"
                    label="Возраст"
                    name="age"
                    autoComplete="age"
                    helperText={formik.touched.age ? formik.errors.age : ''}
                    error={formik.touched.age && Boolean(formik.errors.age)}
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <FormControl fullWidth>
                    <InputLabel id="gender">Пол *</InputLabel>
                    <Select
                      required
                      labelId="gender"
                      id="gender"
                      label="Пол *"
                      value={gender}
                      onChange={handleSelectGender}
                    >
                      <MenuItem value="М">Мужчина</MenuItem>
                      <MenuItem value="Ж">Женщина</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </>
            )}
          </Grid>

          <Divider variant="middle" sx={{ margin: '1.5rem 0' }} />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                name="password"
                label="Новый пароль"
                type="password"
                id="password"
                autoComplete="new-password"
                helperText={formik.errors.password}
                error={Boolean(formik.errors.password)}
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                name="repeatedPassword"
                label="Повтор нового пароля"
                type="password"
                id="repeatedPassword"
                autoComplete="repeated-password"
                helperText={formik.errors.repeatedPassword}
                error={Boolean(formik.errors.repeatedPassword)}
                value={formik.values.repeatedPassword}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </Grid>
          </Grid>

          <Box mt="1em" display="flex" justifyContent="end">
            <Button
              variant="outlined"
              color={color}
              onClick={handleDialogClose}
            >
              Отмена
            </Button>
            <Button
              sx={{ ml: '0.5em' }}
              variant="outlined"
              color={color}
              type="submit"
              disabled={
                Object.keys(formik.errors).length !== 0 || gender === ''
              }
            >
              Сохранить
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

export default ProfileEditDialog;
