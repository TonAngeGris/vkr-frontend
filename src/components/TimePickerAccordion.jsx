/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  TextField,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  Box,
  Grid,
} from '@mui/material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { grey } from '@mui/material/colors';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

dayjs.extend(utc);
dayjs.extend(timezone);

function TimePickerAccordion({ title, shortcuts, value, setValue, color }) {
  const handleStartTime = (newValue, dayOfWeek) => {
    setValue({
      ...value,
      [dayOfWeek]: { ...value[dayOfWeek], startTime: newValue },
    });
  };

  const handleEndTime = (newValue, dayOfWeek) => {
    setValue({
      ...value,
      [dayOfWeek]: { ...value[dayOfWeek], endTime: newValue },
    });
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Accordion
        disableGutters
        sx={{
          boxShadow: 'none',
          bgcolor: 'primary.grey',
        }}
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Box width="17em" display="flex" flexDirection="column">
            {Object.keys(value).map((item) => (
              <Grid key={item} container spacing={0} direction="row" mb="0.5em">
                <Grid item xs={2} alignSelf="center">
                  <Typography
                    textAlign="center"
                    display="flex"
                    alignItems="center"
                    fontSize="18px"
                    color={grey[700]}
                  >
                    {shortcuts[item]}:
                  </Typography>
                </Grid>
                <Grid item xs={10}>
                  <Box display="flex" justifyContent="space-evenly">
                    <TimePicker
                      ampm={false}
                      openTo="hours"
                      views={['hours']}
                      inputFormat="HH"
                      mask="__"
                      label="С"
                      components={{
                        LeftArrowIcon: 'none',
                        RightArrowIcon: 'none',
                      }}
                      value={value[item].startTime}
                      onChange={(newValue) =>
                        handleStartTime(
                          dayjs(newValue).tz('Europe/Moscow').format(),
                          item
                        )
                      }
                      renderInput={(params) => (
                        <TextField
                          color={color}
                          {...params}
                          size="small"
                          sx={{ width: '40%', mr: '0.75em' }}
                        />
                      )}
                    />
                    <TimePicker
                      ampm={false}
                      openTo="hours"
                      views={['hours']}
                      inputFormat="HH"
                      mask="__"
                      label="До"
                      components={{
                        LeftArrowIcon: 'none',
                        RightArrowIcon: 'none',
                      }}
                      defaultValue={dayjs().tz('Europe/Moscow').format('HH:mm')}
                      value={value[item].endTime}
                      onChange={(newValue) =>
                        handleEndTime(
                          dayjs(newValue).tz('Europe/Moscow').format(),
                          item
                        )
                      }
                      renderInput={(params) => (
                        <TextField
                          color={color}
                          {...params}
                          size="small"
                          sx={{ width: '40%' }}
                        />
                      )}
                    />
                  </Box>
                </Grid>
              </Grid>
            ))}
          </Box>
        </AccordionDetails>
      </Accordion>
    </LocalizationProvider>
  );
}

export default TimePickerAccordion;
