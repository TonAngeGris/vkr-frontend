import React, { useEffect, useState } from 'react';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Box,
  TextField,
  Button,
  Autocomplete,
  Grid,
  Typography,
  ButtonGroup,
} from '@mui/material';
import { useFormik } from 'formik';
import debounce from 'lodash.debounce';
import { useDispatch } from 'react-redux';
import { grey } from '@mui/material/colors';
import { LocalizationProvider, TimePicker } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

import createVacancyValidator from '../helpers/createVacancyValidator';
import { setEmployerCreatedVacancy } from '../redux/actions/user';
import { getCities } from '../api/city';
import { getRoles } from '../api/role';
import { createVacancy } from '../api/vacancy';

dayjs.extend(utc);
dayjs.extend(timezone);

const daysOfWeek = {
  monday: 'Пн',
  tuesday: 'Вт',
  wednesday: 'Ср',
  thursday: 'Чт',
  friday: 'Пт',
  sunday: 'Сб',
  saturday: 'Вс',
};

const additionalInfoProperties = {
  foreignPassport: 'Наличие загран паспорта',
  readyForMoving: 'Готовность к переезду',
  readyForTraveling: 'Готовность к командировкам',
  haveChildren: 'Наличие детей',
  driverLicence: 'Наличие водительского удостоверения',
  nonconvictionCertificate: 'Наличие справки о несудимости',
  medicineCertificate: 'Наличие медицинских справок',
  mentalStateCertificate: 'Наличие справки из психдиспансера',
  medicalTests: 'Наличие медицинских тестов',
  smoking: 'Курю',
  canSwimming: 'Умею плавать',
};

function VacancyCreateDialog({
  employerId,
  open,
  handleClose,
  color,
  setError,
}) {
  const [filteredCities, setFilteredCities] = useState([]);
  const [citySearchQuery, setCitySearchQuery] = useState('');

  const [filteredRoles, setFilteredRoles] = useState([]);

  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      role: '',
      city: '',
      experience: '',
      wage: '',
      ageMin: '',
      ageMax: '',
      mondayStartTime: null,
      mondayEndTime: null,
      tuesdayStartTime: null,
      tuesdayEndTime: null,
      wednesdayStartTime: null,
      wednesdayEndTime: null,
      thursdayStartTime: null,
      thursdayEndTime: null,
      fridayStartTime: null,
      fridayEndTime: null,
      sundayStartTime: null,
      sundayEndTime: null,
      saturdayStartTime: null,
      saturdayEndTime: null,
      additionalInfo: {
        smoking: null,
        foreignPassport: null,
        readyForMoving: null,
        readyForTraveling: null,
        haveChildren: null,
        driverLicence: null,
        nonconvictionCertificate: null,
        medicineCertificate: null,
        mentalStateCertificate: null,
        medicalTests: null,
        canSwimming: null,
      },
    },
    validationSchema: createVacancyValidator,
  });

  const handleStartTime = (newValue, dayOfWeek) => {
    formik.setFieldValue(`${[dayOfWeek]}StartTime`, newValue);
  };

  const handleEndTime = (newValue, dayOfWeek) => {
    formik.setFieldValue(`${[dayOfWeek]}EndTime`, newValue);
  };

  const handleShouldDisableStartTime = (item, value, type) => {
    if (formik.values[`${item}EndTime`]) {
      if (type === 'hours' && formik.values[`${item}EndTime`] === 0) {
        return value < 0;
      }
      return type === 'hours' && value >= formik.values[`${item}EndTime`];
    }

    return value < 0;
  };

  const handleShouldDisableEndTime = (item, value, type) => {
    if (formik.values[`${item}StartTime`]) {
      if (type === 'hours' && formik.values[`${item}StartTime`] === 23) {
        return value > 0;
      }
      return (
        type === 'hours' &&
        value <= formik.values[`${item}StartTime`] &&
        value !== 0
      );
    }

    return value < 0;
  };

  const debouncedOnChange = debounce((field, value) => {
    formik.setFieldValue(field, value);
  }, 100);

  const debouncedAdditionalInfoOnChange = debounce((field, value) => {
    formik.setFieldValue(`additionalInfo.${field}`, value);
  }, 100);

  const handleChange = (event) => {
    const { name, value } = event.target;
    debouncedOnChange(name, value);
  };

  const handleDialogClose = () => {
    handleClose();
    // Убираем ошибки и введенные данные при следующем открытии
    formik.setTouched({}, false);
    formik.setValues(formik.initialValues);
    setFilteredCities([]);
  };

  useEffect(() => {
    async function fetchRoles() {
      const fetchedRoles = await getRoles();

      setFilteredRoles(fetchedRoles);
    }

    if (open) {
      fetchRoles();
    }
  }, [open]);

  const handleCitySearch = debounce(async (query) => {
    const fetchedData = await getCities({ query });
    setFilteredCities(fetchedData);
    setCitySearchQuery(query);
  }, 200);

  const handleCityInputChange = (_, value) => {
    if (value.trim() === '' || value.trim().length < 3) {
      setFilteredCities([]);
      setCitySearchQuery('');
      return;
    }

    handleCitySearch(value.trim());
  };

  const handleRoleSearch = debounce(async (query) => {
    const fetchedData = await getRoles({ query });
    setFilteredRoles(fetchedData);
  }, 200);

  const handleRoleInputChange = (_, value) => {
    handleRoleSearch(value);
  };

  const handleOnFocusAutocomplete = () => {
    if (filteredCities) setFilteredCities([]);
    if (citySearchQuery) setCitySearchQuery('');
  };

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    const bodyData = {
      ...formik.values,
      employerId,
    };

    try {
      const data = await createVacancy(bodyData);
      dispatch(setEmployerCreatedVacancy({ createdVacancy: data }));
    } catch (error) {
      setError(error?.response?.data?.message);
      formik.setValues(formik.initialValues);
    }

    handleClose();
  };

  // TODO: сделать написать времени в графике без задержки
  // (то же самое и для обновления и для двуз компонентов резюме)
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Dialog
        open={open}
        onClose={handleDialogClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth={false}
        PaperProps={{ sx: { width: '65rem', height: '85%' } }}
      >
        <DialogTitle id="alert-dialog-title">Размещение вакансии</DialogTitle>

        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Чтобы разместить вакансию - просто заполните необходимые поля без
            ошибок и нажмите кнопку &apos;&apos;Разместить&apos;&apos;
          </DialogContentText>

          <Box
            component="form"
            noValidate
            onSubmit={handleSubmitForm}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2} justifyContent="space-between">
              <Grid container item spacing={2} xs={12} sm={6}>
                <Grid item xs={12} sm={12}>
                  <TextField
                    fullWidth
                    required
                    id="title"
                    label="Заголовок"
                    name="title"
                    autoComplete="title"
                    helperText={formik.touched.title ? formik.errors.title : ''}
                    error={formik.touched.title && Boolean(formik.errors.title)}
                    onChange={handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>

                <Grid item xs={12} sm={12}>
                  <TextField
                    fullWidth
                    required
                    multiline
                    id="description"
                    label="Описание"
                    name="description"
                    autoComplete="description"
                    helperText={
                      formik.touched.description
                        ? formik.errors.description
                        : ''
                    }
                    error={
                      formik.touched.description &&
                      Boolean(formik.errors.description)
                    }
                    onChange={handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>

                <Grid item xs={12} sm={12}>
                  <Autocomplete
                    freeSolo={Boolean(citySearchQuery.length < 3)}
                    id="city"
                    noOptionsText="Ничего не найдено"
                    options={filteredCities}
                    onInputChange={handleCityInputChange}
                    onFocus={handleOnFocusAutocomplete}
                    name="city"
                    autoComplete
                    value={formik.values.city}
                    onChange={(_, value) => formik.setFieldValue('city', value)}
                    onBlur={formik.handleBlur}
                    renderInput={(params) => (
                      <TextField
                        // eslint-disable-next-line react/jsx-props-no-spreading
                        {...params}
                        label="Город проживания *"
                        helperText={
                          formik.touched.city ? formik.errors.city : ''
                        }
                        error={
                          formik.touched.city && Boolean(formik.errors.city)
                        }
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <Autocomplete
                    id="role"
                    noOptionsText="Ничего не найдено"
                    options={filteredRoles}
                    onInputChange={handleRoleInputChange}
                    onFocus={handleOnFocusAutocomplete}
                    name="role"
                    autoComplete
                    value={formik.values.role}
                    onChange={(_, value) => formik.setFieldValue('role', value)}
                    onBlur={formik.handleBlur}
                    renderInput={(params) => (
                      <TextField
                        // eslint-disable-next-line react/jsx-props-no-spreading
                        {...params}
                        label="Специальность *"
                        helperText={
                          formik.touched.role ? formik.errors.role : ''
                        }
                        error={
                          formik.touched.role && Boolean(formik.errors.role)
                        }
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={12} sm={3}>
                  <TextField
                    fullWidth
                    id="ageMin"
                    label="Мин. лет"
                    name="ageMin"
                    autoComplete="ageMin"
                    helperText={
                      formik.touched.ageMin ? formik.errors.ageMin : ''
                    }
                    error={
                      formik.touched.ageMin && Boolean(formik.errors.ageMin)
                    }
                    onChange={handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>

                <Grid item xs={12} sm={3}>
                  <TextField
                    fullWidth
                    id="ageMax"
                    label="Макс. лет"
                    name="ageMax"
                    autoComplete="ageMax"
                    helperText={
                      formik.touched.ageMax ? formik.errors.ageMax : ''
                    }
                    error={
                      formik.touched.ageMax && Boolean(formik.errors.ageMax)
                    }
                    onChange={handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id="experience"
                    label="Стаж, лет"
                    name="experience"
                    autoComplete="experience"
                    helperText={
                      formik.touched.experience ? formik.errors.experience : ''
                    }
                    error={
                      formik.touched.experience &&
                      Boolean(formik.errors.experience)
                    }
                    onChange={handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    fullWidth
                    id="wage"
                    label="Зарплата, ₽"
                    name="wage"
                    autoComplete="wage"
                    helperText={formik.touched.wage ? formik.errors.wage : ''}
                    error={formik.touched.wage && Boolean(formik.errors.wage)}
                    onChange={handleChange}
                    onBlur={formik.handleBlur}
                  />
                </Grid>

                <Grid item xs={12} sm={12}>
                  <DialogContentText id="alert-dialog-description">
                    График по часам:
                  </DialogContentText>
                </Grid>

                <Grid item xs={12} sm={6}>
                  {Object.keys(daysOfWeek)
                    .filter((_, idx) => idx < 4)
                    .map((item) => (
                      <Grid
                        key={item}
                        container
                        spacing={0}
                        direction="row"
                        mb="0.5em"
                      >
                        <Grid
                          item
                          xs={2}
                          alignSelf="center"
                          justifySelf="start"
                        >
                          <Typography
                            textAlign="center"
                            display="flex"
                            alignItems="center"
                            color={grey[700]}
                          >
                            {daysOfWeek[item]}:
                          </Typography>
                        </Grid>

                        <Grid item xs={10}>
                          <Box display="flex" justifyContent="space-evenly">
                            <TimePicker
                              ampm={false}
                              openTo="hours"
                              views={['hours']}
                              inputFormat="H"
                              mask="__"
                              label="С"
                              value={
                                formik.values[`${item}StartTime`] === null
                                  ? null
                                  : dayjs().set(
                                      'hour',
                                      formik.values[`${item}StartTime`]
                                    )
                              }
                              onChange={(newValue) => {
                                if (
                                  newValue !== null &&
                                  newValue?.hour() !== null
                                ) {
                                  handleStartTime(newValue.hour(), item);
                                } else {
                                  handleStartTime(null, item);
                                }
                              }}
                              shouldDisableTime={(value, type) =>
                                handleShouldDisableStartTime(item, value, type)
                              }
                              components={{
                                LeftArrowIcon: 'none',
                                RightArrowIcon: 'none',
                              }}
                              renderInput={(params) => (
                                <TextField
                                  color={color}
                                  // eslint-disable-next-line react/jsx-props-no-spreading
                                  {...params}
                                  error={Boolean(
                                    formik.errors[`${item}StartTime`]
                                  )}
                                  size="small"
                                  sx={{
                                    width: '80%',
                                    mr: '1em',
                                  }}
                                />
                              )}
                            />
                            <TimePicker
                              ampm={false}
                              openTo="hours"
                              views={['hours']}
                              inputFormat="H"
                              mask="__"
                              label="До"
                              value={
                                formik.values[`${item}EndTime`] === null
                                  ? null
                                  : dayjs().set(
                                      'hour',
                                      formik.values[`${item}EndTime`]
                                    )
                              }
                              onChange={(newValue) => {
                                if (
                                  newValue !== null &&
                                  newValue?.hour() !== null
                                ) {
                                  handleEndTime(newValue.hour(), item);
                                } else {
                                  handleEndTime(null, item);
                                }
                              }}
                              shouldDisableTime={(value, type) =>
                                handleShouldDisableEndTime(item, value, type)
                              }
                              components={{
                                LeftArrowIcon: 'none',
                                RightArrowIcon: 'none',
                              }}
                              renderInput={(params) => (
                                <TextField
                                  color={color}
                                  // eslint-disable-next-line react/jsx-props-no-spreading
                                  {...params}
                                  error={Boolean(
                                    formik.errors[`${item}EndTime`]
                                  )}
                                  size="small"
                                  sx={{
                                    width: '80%',
                                  }}
                                />
                              )}
                            />
                          </Box>
                        </Grid>
                      </Grid>
                    ))}
                </Grid>

                <Grid item xs={12} sm={6}>
                  {Object.keys(daysOfWeek)
                    .filter((_, idx) => idx >= 4)
                    .map((item) => (
                      <Grid
                        key={item}
                        container
                        spacing={0}
                        direction="row"
                        mb="0.5em"
                      >
                        <Grid
                          item
                          xs={2}
                          alignSelf="center"
                          justifySelf="start"
                        >
                          <Typography
                            textAlign="center"
                            display="flex"
                            alignItems="center"
                            color={grey[700]}
                          >
                            {daysOfWeek[item]}:
                          </Typography>
                        </Grid>

                        <Grid item xs={10}>
                          <Box display="flex" justifyContent="space-evenly">
                            <TimePicker
                              ampm={false}
                              openTo="hours"
                              views={['hours']}
                              inputFormat="H"
                              mask="__"
                              label="С"
                              value={
                                formik.values[`${item}StartTime`] === null
                                  ? null
                                  : dayjs().set(
                                      'hour',
                                      formik.values[`${item}StartTime`]
                                    )
                              }
                              onChange={(newValue) => {
                                if (
                                  newValue !== null &&
                                  newValue?.hour() !== null
                                ) {
                                  handleStartTime(newValue.hour(), item);
                                } else {
                                  handleStartTime(null, item);
                                }
                              }}
                              shouldDisableTime={(value, type) =>
                                handleShouldDisableStartTime(item, value, type)
                              }
                              components={{
                                LeftArrowIcon: 'none',
                                RightArrowIcon: 'none',
                              }}
                              renderInput={(params) => (
                                <TextField
                                  color={color}
                                  // eslint-disable-next-line react/jsx-props-no-spreading
                                  {...params}
                                  size="small"
                                  error={Boolean(
                                    formik.errors[`${item}StartTime`]
                                  )}
                                  sx={{
                                    width: '80%',
                                    mr: '1em',
                                  }}
                                />
                              )}
                            />
                            <TimePicker
                              ampm={false}
                              openTo="hours"
                              views={['hours']}
                              inputFormat="H"
                              mask="__"
                              label="До"
                              value={
                                formik.values[`${item}EndTime`] === null
                                  ? null
                                  : dayjs().set(
                                      'hour',
                                      formik.values[`${item}EndTime`]
                                    )
                              }
                              onChange={(newValue) => {
                                if (
                                  newValue !== null &&
                                  newValue?.hour() !== null
                                ) {
                                  handleEndTime(newValue.hour(), item);
                                } else {
                                  handleEndTime(null, item);
                                }
                              }}
                              shouldDisableTime={(value, type) =>
                                handleShouldDisableEndTime(item, value, type)
                              }
                              components={{
                                LeftArrowIcon: 'none',
                                RightArrowIcon: 'none',
                              }}
                              renderInput={(params) => (
                                <TextField
                                  color={color}
                                  // eslint-disable-next-line react/jsx-props-no-spreading
                                  {...params}
                                  error={Boolean(
                                    formik.errors[`${item}EndTime`]
                                  )}
                                  size="small"
                                  sx={{
                                    width: '80%',
                                  }}
                                />
                              )}
                            />
                          </Box>
                        </Grid>
                      </Grid>
                    ))}
                </Grid>
              </Grid>

              <Grid item gap="10em">
                {Object.keys(formik.values.additionalInfo).map((item) => (
                  <Box
                    key={item}
                    mb="0.9em"
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Typography variant="body1" mr="1em">
                      {additionalInfoProperties[item]}
                    </Typography>
                    <ButtonGroup
                      disableElevation
                      variant="outlined"
                      color={color}
                    >
                      <Button
                        variant={
                          formik.values.additionalInfo[item]
                            ? 'contained'
                            : 'outlined'
                        }
                        onClick={() =>
                          debouncedAdditionalInfoOnChange([item], true)
                        }
                      >
                        Да
                      </Button>
                      <Button
                        variant={
                          formik.values.additionalInfo[item] === null
                            ? 'contained'
                            : 'outlined'
                        }
                        onClick={() =>
                          debouncedAdditionalInfoOnChange([item], null)
                        }
                      >
                        —
                      </Button>
                      <Button
                        variant={
                          formik.values.additionalInfo[item] === false
                            ? 'contained'
                            : 'outlined'
                        }
                        onClick={() =>
                          debouncedAdditionalInfoOnChange([item], false)
                        }
                      >
                        Нет
                      </Button>
                    </ButtonGroup>
                  </Box>
                ))}
              </Grid>
            </Grid>

            <Box mt="1em" display="flex" justifyContent="end">
              <Button
                variant="outlined"
                color={color}
                onClick={handleDialogClose}
              >
                Отмена
              </Button>
              <Button
                sx={{ ml: '0.5em' }}
                variant="outlined"
                color={color}
                type="submit"
                disabled={
                  Object.keys(formik.errors).length !== 0 ||
                  !formik.values.title
                }
              >
                Создать
              </Button>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </LocalizationProvider>
  );
}

export default VacancyCreateDialog;
