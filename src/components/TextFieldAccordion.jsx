import React from 'react';
import {
  Box,
  TextField,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

function TextFieldAccordion({ title, description, value, setValue, color }) {
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Accordion
      disableGutters
      sx={{
        boxShadow: 'none',
        bgcolor: 'primary.grey',
      }}
    >
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Box width="17em" display="flex" justifyContent="space-evenly" px="1em">
          <TextField
            color={color}
            label={description || title}
            value={value}
            onChange={handleChange}
          />
        </Box>
      </AccordionDetails>
    </Accordion>
  );
}
export default TextFieldAccordion;
