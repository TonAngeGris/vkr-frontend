import {
  Box,
  Button,
  Chip,
  Paper,
  Rating,
  Typography,
  Breadcrumbs,
  Avatar,
  Tooltip,
  Snackbar,
  Alert,
  Skeleton,
} from '@mui/material';
import React, { useState } from 'react';
import PlaceIcon from '@mui/icons-material/Place';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import 'dayjs/locale/ru';
import { useDispatch } from 'react-redux';

import {
  deleteEmployerPendingAssignmentContract,
  deleteWorkerPendingAssignmentContract,
  setEmployerAssignedContract,
  setWorkerAssignedContract,
} from '../redux/actions/user';
import { deleteContract, updateContract } from '../api/contract';
import ContractCompleteDialog from './ContractCompleteDialog';

function ContractCard({
  isWorker,
  isEmployer,
  id,
  workerId,
  employerId,
  title,
  wage,
  role,
  city,
  startDate,
  endDate,
  workerAssigned,
  employerAssigned,
  workerCompletionAgreement,
  employerCompletionAgreement,
  workerRatingByEmployer,
  employerRatingByWorker,
  workerComment,
  employerComment,
  workerFirstName,
  workerLastName,
  employerFirstName,
  employerLastName,
  color,
}) {
  const [contractCompleteDialogOpen, setContractCompleteDialogOpen] =
    useState(false);

  const [updateError, setUpdateError] = useState('');

  const dispatch = useDispatch();

  const handleContractCompleteDialogOpen = () => {
    setContractCompleteDialogOpen(true);
  };

  const handleContractCompleteDialogClose = () => {
    setContractCompleteDialogOpen(false);
  };

  const handleCloseSnackbar = (_, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setUpdateError('');
  };

  const handleRejectContract = async (event) => {
    event.preventDefault();

    await deleteContract(id);

    if (isWorker) {
      dispatch(deleteWorkerPendingAssignmentContract({ id }));
    } else if (isEmployer) {
      dispatch(deleteEmployerPendingAssignmentContract({ id }));
    }
  };

  const handleAcceptContract = async (event) => {
    event.preventDefault();

    let data;
    if (isWorker) {
      data = await updateContract({ workerAssigned: true }, id);
      dispatch(deleteWorkerPendingAssignmentContract({ id }));
      dispatch(setWorkerAssignedContract({ assignedContract: data }));
    } else if (isEmployer) {
      data = await updateContract({ employerAssigned: true }, id);
      dispatch(deleteEmployerPendingAssignmentContract({ id }));
      dispatch(setEmployerAssignedContract({ assignedContract: data }));
    }
  };

  const formattedWage = wage
    ? wage.toLocaleString('ru-RU', {
        style: 'currency',
        currency: 'RUB',
      })
    : 'Договорная';

  const dateNow = dayjs();
  const startDateFormatted = dayjs(
    `${startDate.slice(5, 7)}.${startDate.slice(-2)}.${startDate.slice(0, 4)}`
  );

  const startDateRuFormat = dayjs(startDate).format('DD.MM.YY');
  const endDateRuFormat = dayjs(endDate).format('DD.MM.YY');

  const contractDays = dateNow.diff(startDateFormatted, 'd');

  let buttonOrStatus = (
    <Tooltip
      arrow
      title={contractDays < 1 && 'Нельзя завершить договор в день его начала'}
    >
      <Box>
        <Button
          disabled={contractDays < 1}
          variant="outlined"
          size="small"
          color={color}
          onClick={handleContractCompleteDialogOpen}
        >
          Завершить
        </Button>
      </Box>
    </Tooltip>
  );

  switch (true) {
    case workerCompletionAgreement && employerCompletionAgreement: {
      buttonOrStatus = (
        <Button disabled variant="outlined" size="small">
          Завершён
        </Button>
      );
      break;
    }

    case workerCompletionAgreement && !employerCompletionAgreement: {
      buttonOrStatus = (
        <Box display="flex" flexDirection="column">
          {isWorker && (
            <>
              <Typography textAlign="center" variant="body2">
                Ожидает завершение от работодателя
              </Typography>
              <Button
                disabled
                variant="outlined"
                size="small"
                sx={{
                  mt: '0.5em',
                  alignSelf: 'center',
                  width: '50%',
                }}
              >
                Завершён
              </Button>
            </>
          )}

          {isEmployer && (
            <>
              <Typography textAlign="center" variant="body2">
                Работник ожидает от вас завершение
              </Typography>
              <Button
                variant="outlined"
                size="small"
                color={color}
                sx={{
                  mt: '0.5em',
                  alignSelf: 'center',
                  width: '55%',
                }}
                onClick={handleContractCompleteDialogOpen}
              >
                Завершить
              </Button>
            </>
          )}
        </Box>
      );
      break;
    }

    case !workerCompletionAgreement && employerCompletionAgreement: {
      buttonOrStatus = (
        <Box display="flex" flexDirection="column">
          {isEmployer && (
            <>
              <Typography textAlign="center" variant="body2">
                Работодатель ожидает от вас завершение
              </Typography>
              <Button
                disabled
                variant="outlined"
                size="small"
                sx={{
                  mt: '0.5em',
                  alignSelf: 'center',
                  width: '55%',
                }}
              >
                Завершён
              </Button>
            </>
          )}

          {isWorker && (
            <>
              <Typography textAlign="center" variant="body2">
                Договор ожидает от Вас завершение
              </Typography>
              <Button
                variant="outlined"
                size="small"
                color={color}
                sx={{
                  mt: '0.5em',
                  alignSelf: 'center',
                  width: '60%',
                }}
                onClick={handleContractCompleteDialogOpen}
              >
                Завершить
              </Button>
            </>
          )}
        </Box>
      );
      break;
    }

    case (workerAssigned && !employerAssigned && isEmployer) ||
      (!workerAssigned && employerAssigned && isWorker): {
      buttonOrStatus = (
        <Box display="flex" flexDirection="column">
          <Button
            variant="outlined"
            size="small"
            sx={{ mb: '0.5em', color: 'green', borderColor: 'green' }}
            onClick={handleAcceptContract}
          >
            Принять
          </Button>
          <Button
            variant="outlined"
            size="small"
            sx={{ color: 'crimson', borderColor: 'crimson' }}
            onClick={handleRejectContract}
          >
            Отклонить
          </Button>
        </Box>
      );
      break;
    }

    case workerAssigned && !employerAssigned: {
      buttonOrStatus = (
        <Box display="flex" flexDirection="column">
          <Typography textAlign="center" variant="body2">
            Ожидает подтверждение от работодателя
          </Typography>
          {isWorker && (
            <Button
              variant="outlined"
              size="small"
              onClick={handleRejectContract}
              sx={{
                mt: '0.5em',
                color: 'crimson',
                borderColor: 'crimson',
                alignSelf: 'center',
                width: '50%',
              }}
            >
              Отклонить
            </Button>
          )}
        </Box>
      );
      break;
    }

    case !workerAssigned && employerAssigned: {
      buttonOrStatus = (
        <Box display="flex" flexDirection="column">
          <Typography textAlign="center" variant="body2">
            Ожидает подтверждение от работника
          </Typography>
          {isEmployer && (
            <Button
              variant="outlined"
              size="small"
              onClick={handleRejectContract}
              sx={{
                mt: '0.5em',
                color: 'crimson',
                borderColor: 'crimson',
                alignSelf: 'center',
                width: '50%',
              }}
            >
              Отклонить
            </Button>
          )}
        </Box>
      );
      break;
    }

    default:
      break;
  }

  return (
    <Paper variant="outlined" sx={{ mb: '0.75em', p: '1em', width: '300%' }}>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Typography variant="h5" sx={{ fontWeight: 700 }}>
          {title}
        </Typography>
        {workerAssigned && employerAssigned ? (
          <Breadcrumbs separator="—" aria-label="breadcrumb">
            <Chip label={startDateRuFormat} />
            {workerCompletionAgreement && employerCompletionAgreement ? (
              <Chip label={endDateRuFormat} />
            ) : (
              <Chip label="В работе" />
            )}
          </Breadcrumbs>
        ) : (
          <Chip label={startDateRuFormat} />
        )}
      </Box>

      <Box display="flex" color="primary.darkgrey">
        <PlaceIcon fontSize="small" />
        <Typography mt="0.1em" variant="body2">
          {city}
        </Typography>
      </Box>

      <Box display="flex" my="0.5em">
        <Box mr="2em">
          <Typography variant="body2" color="primary.darkgrey">
            Зарплата
          </Typography>
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {formattedWage}
          </Typography>
        </Box>
        <Box mr="2em">
          <Typography variant="body2" color="primary.darkgrey">
            Роль
          </Typography>
          <Typography variant="subtitle1" sx={{ fontWeight: 700 }}>
            {role}
          </Typography>
        </Box>
        {isWorker && (
          <>
            <Box width="50%">
              <>
                <Typography variant="body2" color="primary.darkgrey">
                  Наниматель
                </Typography>
                <Link
                  to={`/employers/${employerId}`}
                  style={{ textDecoration: 'none' }}
                >
                  <Box display="flex" alignItems="center">
                    <Avatar
                      sx={{
                        bgcolor: 'primary.main',
                        fontSize: '12px',
                        fontWeight: 600,
                        width: 24,
                        height: 24,
                      }}
                    >
                      {employerFirstName?.[0]}
                      {employerLastName?.[0]}
                    </Avatar>
                    <Typography
                      mx="0.5em"
                      variant="body2"
                      color="black"
                      sx={{ fontWeight: 500 }}
                    >
                      {employerFirstName} {employerLastName}
                    </Typography>
                  </Box>
                </Link>
              </>
            </Box>

            <Box ml="auto" display="flex" alignItems="start">
              {buttonOrStatus}
            </Box>
          </>
        )}

        {isEmployer && (
          <>
            <Box width="50%">
              <>
                <Typography variant="body2" color="primary.darkgrey">
                  Работник
                </Typography>
                <Link
                  to={`/workers/${workerId}`}
                  style={{ textDecoration: 'none' }}
                >
                  <Box display="flex" alignItems="center">
                    <Avatar
                      sx={{
                        bgcolor: 'secondary.main',
                        fontSize: '12px',
                        fontWeight: 600,
                        width: 24,
                        height: 24,
                      }}
                    >
                      {workerFirstName?.[0]}
                      {workerLastName?.[0]}
                    </Avatar>
                    <Typography
                      mx="0.5em"
                      variant="body2"
                      color="black"
                      sx={{ fontWeight: 500 }}
                    >
                      {workerFirstName} {workerLastName}
                    </Typography>
                  </Box>
                </Link>
              </>
            </Box>

            <Box ml="auto" display="flex" alignItems="start">
              {buttonOrStatus}
            </Box>
          </>
        )}
      </Box>

      {/* TODO:? Добавить проверку получше */}
      {isWorker && workerRatingByEmployer && employerCompletionAgreement && (
        <>
          <Box>
            <Typography variant="body2" color="primary.darkgrey">
              Комментарий работодателя:
            </Typography>
            {workerCompletionAgreement && employerCompletionAgreement ? (
              <Typography variant="body2">{employerComment}</Typography>
            ) : (
              <Skeleton
                animation={false}
                width={`calc(0.55em * ${employerComment.length})`}
              />
            )}
          </Box>
          <Box mt="0.5em">
            <Typography variant="body2" color="primary.darkgrey">
              Оценка:
            </Typography>
            {workerCompletionAgreement && employerCompletionAgreement ? (
              <Box display="flex" alignItems="center">
                <Rating
                  name="read-only"
                  value={Number(workerRatingByEmployer)}
                  precision={0.5}
                  readOnly
                />
                <Typography ml="0.5em" variant="body1" fontWeight="bold">
                  {Math.round(Number(workerRatingByEmployer) * 10) / 10}
                </Typography>
              </Box>
            ) : (
              <Skeleton animation={false} width="10em" height="2em" />
            )}
          </Box>
        </>
      )}

      {/* TODO:? Добавить проверку получше */}
      {isEmployer && employerRatingByWorker && workerCompletionAgreement && (
        <>
          <Box>
            <Typography variant="body2" color="primary.darkgrey">
              Комментарий работника:
            </Typography>
            {workerCompletionAgreement && employerCompletionAgreement ? (
              <Typography variant="body2">{workerComment}</Typography>
            ) : (
              <Skeleton
                animation={false}
                width={`calc(0.55em * ${workerComment.length})`}
              />
            )}
          </Box>
          <Box mt="0.5em">
            <Typography variant="body2" color="primary.darkgrey">
              Оценка:
            </Typography>
            {workerCompletionAgreement && employerCompletionAgreement ? (
              <Box display="flex" alignItems="center">
                <Rating
                  name="read-only"
                  value={Number(employerRatingByWorker)}
                  precision={0.5}
                  readOnly
                />
                <Typography ml="0.5em" variant="body1" fontWeight="bold">
                  {Math.round(Number(employerRatingByWorker) * 10) / 10}
                </Typography>
              </Box>
            ) : (
              <Skeleton animation={false} width="10em" height="2em" />
            )}
          </Box>
        </>
      )}

      <Snackbar
        open={Boolean(updateError)}
        autoHideDuration={5000}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        onClose={handleCloseSnackbar}
      >
        <Alert
          severity="error"
          sx={{ width: '100%' }}
          onClose={handleCloseSnackbar}
        >
          {updateError}
        </Alert>
      </Snackbar>

      <ContractCompleteDialog
        open={contractCompleteDialogOpen}
        handleClose={handleContractCompleteDialogClose}
        color={color}
        setError={setUpdateError}
        contractData={{
          id,
          workerId,
          employerId,
          workerCompletionAgreement,
          employerCompletionAgreement,
        }}
        isWorker={isWorker}
        isEmployer={isEmployer}
      />
    </Paper>
  );
}

export default ContractCard;
