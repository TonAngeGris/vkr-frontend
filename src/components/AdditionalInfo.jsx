import React from 'react';
import { Typography, Box, Button, Divider } from '@mui/material';
import SchoolIcon from '@mui/icons-material/School';
import BookIcon from '@mui/icons-material/Book';
import LuggageIcon from '@mui/icons-material/Luggage';
import RecentActorsIcon from '@mui/icons-material/RecentActors';
import MedicalInformationIcon from '@mui/icons-material/MedicalInformation';
import TextSnippetIcon from '@mui/icons-material/TextSnippet';
import DriveFileRenameOutlineRoundedIcon from '@mui/icons-material/DriveFileRenameOutlineRounded';
import MenuIcon from '@mui/icons-material/Menu';
import { grey } from '@mui/material/colors';
import { useNavigate } from 'react-router-dom';

function AdditionalInfo({ isAccountOwner, isAuth, workerData, handleOpen }) {
  const navigate = useNavigate();

  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Typography variant="h6">Подробная информация</Typography>
        {isAccountOwner && (
          <Button
            size="small"
            color="secondary"
            variant="outlined"
            onClick={handleOpen}
            startIcon={<DriveFileRenameOutlineRoundedIcon />}
          >
            Редактировать
          </Button>
        )}

        {!isAuth && (
          <Button
            color="secondary"
            variant="outlined"
            size="small"
            endIcon={<MenuIcon />}
            onClick={() => navigate('/sign-in')}
          >
            Посмотреть
          </Button>
        )}
      </Box>

      {Boolean(isAuth) && (
        <>
          <Box display="flex" color="primary.darkgrey" mt="0.5em">
            <Box
              display="flex"
              bgcolor={grey[100]}
              p="0.5em"
              pr="0.75em"
              borderRadius="10px"
            >
              <SchoolIcon fontSize="small" />
              <Box ml="0.5em">
                <Typography>
                  <b>Образование</b>
                </Typography>
                {workerData.additional_info?.educations.length ? (
                  workerData.additional_info?.educations?.map((item) => (
                    <Typography key={item.education_branch} variant="body2">
                      {item.education_branch}
                    </Typography>
                  ))
                ) : (
                  <Typography variant="body2">Нет</Typography>
                )}
              </Box>
            </Box>

            <Box
              display="flex"
              ml="1em"
              bgcolor={grey[100]}
              p="0.5em"
              borderRadius="10px"
              flex={1}
            >
              <BookIcon fontSize="small" />
              <Box ml="0.5em">
                <Typography>
                  <b>Загран паспорт</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.foreign_passport
                    ? 'Есть'
                    : 'Нет'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Иностранное гражданство</b>
                </Typography>
                {workerData.additional_info?.citizenships.length ? (
                  workerData.additional_info?.citizenships?.map((item) => (
                    <Typography key={item.citizenship_name} variant="body2">
                      {item.citizenship_name}
                    </Typography>
                  ))
                ) : (
                  <Typography variant="body2">Нет</Typography>
                )}
                <Typography mt="0.5em">
                  <b>Владение ин. языками</b>
                </Typography>
                {workerData.additional_info?.languages.length ? (
                  workerData.additional_info?.languages?.map((item) => (
                    <Typography key={item.language_name} variant="body2">
                      {item.language_name}
                    </Typography>
                  ))
                ) : (
                  <Typography variant="body2">Нет</Typography>
                )}
              </Box>
            </Box>

            <Box
              display="flex"
              ml="1em"
              bgcolor={grey[100]}
              p="0.5em"
              pr="0.75em"
              borderRadius="10px"
            >
              <LuggageIcon fontSize="small" />
              <Box ml="0.5em">
                <Typography>
                  <b>Переезд</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.ready_for_moving
                    ? 'Готов(а)'
                    : 'Не готов(а)'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Командировки</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.ready_for_traveling
                    ? 'Готов(а)'
                    : 'Не готов(а)'}
                </Typography>
              </Box>
            </Box>
          </Box>

          <Divider sx={{ mt: '1em', borderBottomWidth: 1 }} />

          <Box display="flex" color="primary.darkgrey" mt="1em">
            <Box
              display="flex"
              bgcolor={grey[100]}
              p="0.5em"
              pr="0.75em"
              borderRadius="10px"
            >
              <RecentActorsIcon fontSize="small" />
              <Box ml="0.5em">
                <Typography>
                  <b>Водительские права</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.driver_licence ? 'Есть' : 'Нет'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Справка о несудимости</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.nonconviction_certificate
                    ? 'Есть'
                    : 'Нет'}
                </Typography>
              </Box>
            </Box>

            <Box
              display="flex"
              ml="1em"
              bgcolor={grey[100]}
              p="0.5em"
              pr="0.75em"
              borderRadius="10px"
            >
              <MedicalInformationIcon fontSize="small" />
              <Box ml="0.5em">
                <Typography>
                  <b>Справка из психдиспансера</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.mental_state_certificate
                    ? 'Есть'
                    : 'Нет'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Медицинские справки</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.medicine_certificate
                    ? 'Есть'
                    : 'Нет'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Медицинские тесты</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.medical_tests ? 'Есть' : 'Нет'}
                </Typography>
              </Box>
            </Box>

            <Box
              display="flex"
              ml="1em"
              bgcolor={grey[100]}
              p="0.5em"
              pr="0.75em"
              borderRadius="10px"
            >
              <TextSnippetIcon fontSize="small" />
              <Box ml="0.5em">
                <Typography>
                  <b>Есть дети</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.have_children ? 'Да' : 'Нет'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Курю</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.smoking ? 'Да' : 'Нет'}
                </Typography>
                <Typography mt="0.5em">
                  <b>Умею плавать</b>
                </Typography>
                <Typography variant="body2">
                  {workerData.additional_info?.can_swimming ? 'Да' : 'Нет'}
                </Typography>
              </Box>
            </Box>
          </Box>
        </>
      )}
    </>
  );
}

export default AdditionalInfo;
