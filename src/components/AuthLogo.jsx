import React from 'react';
import { Avatar, Box, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

function AuthLogo({ title }) {
  return (
    <Box display="flex" alignItems="center" mb="0.5em">
      <Avatar sx={{ margin: '0.5rem', bgcolor: 'secondary.main' }}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        {title}
      </Typography>
    </Box>
  );
}

export default AuthLogo;
