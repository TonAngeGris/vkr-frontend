import React, { useState } from 'react';
import {
  AppBar,
  Toolbar,
  Avatar,
  Button,
  Box,
  Typography,
  Container,
  IconButton,
  Menu,
  MenuItem,
  Tooltip,
  Skeleton,
} from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import MenuIcon from '@mui/icons-material/Menu';
import CleaningServicesIcon from '@mui/icons-material/CleaningServices';

import { resetAuthData } from '../redux/actions/auth';
import {
  readUserTypeFromLS,
  removeRefreshTokenFromLS,
  removeUserTypeFromLS,
} from '../lib/localStorage';

const BASE_AVATAR_URL = import.meta.env.VITE_AVATAR_URL;

function Navbar() {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleOnLogout = () => {
    dispatch(resetAuthData());
    removeRefreshTokenFromLS();
    removeUserTypeFromLS();
    setAnchorElUser(null);
    navigate('/');
  };

  const { authUser, fetching: isAuthFetching } = useSelector(
    (state) => state.authReducer
  );

  const { userData } = useSelector((state) => state.userReducer);

  const isAuth = Object.keys(authUser).length;
  const userGroup = readUserTypeFromLS();

  let rightButtons = <Skeleton width="8em" height="2.5em" />;

  if (isAuth) {
    rightButtons = (
      <>
        <Tooltip title="Открыть меню">
          <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
            <Typography
              variant="h6"
              noWrap
              sx={{
                display: { xs: 'none', md: 'flex' },
                mr: 2,
                fontWeight: 400,
                color: 'white',
                textDecoration: 'none',
              }}
            >
              {`${authUser?.firstname} ${authUser?.lastname[0]}.`}
            </Typography>

            {userData?.employerContracts?.pending_assignment?.length ||
            userData?.workerContracts?.pending_assignment?.length ? (
              // <Badge
              //   overlap="circular"
              //   color="red"
              //   badgeContent={badgeCount || null}
              //   sx={{ color: 'white' }}
              // >
              <Avatar
                src={`${BASE_AVATAR_URL}/${authUser.avatar}`}
                sx={{
                  bgcolor: 'white',
                  fontSize: '21px',
                }}
              >
                {authUser.firstname[0]}
                {authUser.lastname[0]}
              </Avatar>
            ) : (
              // </Badge>
              <Avatar
                src={`${BASE_AVATAR_URL}/${authUser.avatar}`}
                sx={{
                  bgcolor: 'white',
                  fontSize: '21px',
                }}
              >
                {authUser.firstname[0]}
                {authUser.lastname[0]}
              </Avatar>
            )}
          </IconButton>
        </Tooltip>

        <Menu
          sx={{ mt: '45px' }}
          id="menu-appbar"
          anchorEl={anchorElUser}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={Boolean(anchorElUser)}
          onClose={handleCloseUserMenu}
        >
          <MenuItem onClick={handleCloseUserMenu}>
            <Link
              to={`/${userGroup}/${authUser.id}`}
              style={{ textDecoration: 'none', color: 'black' }}
            >
              <Typography textAlign="center">Профиль</Typography>
            </Link>
          </MenuItem>
          <MenuItem onClick={handleOnLogout}>
            <Typography textAlign="center">Выйти</Typography>
          </MenuItem>
        </Menu>
      </>
    );
  } else if (!isAuth && !isAuthFetching) {
    rightButtons = (
      <>
        <Button variant="outlined" sx={{ borderColor: 'white' }}>
          <Link
            to="/sign-in"
            style={{ textDecoration: 'none', color: 'white' }}
          >
            Вход
          </Link>
        </Button>
        <Button
          variant="outlined"
          sx={{
            borderColor: 'white',
            marginLeft: '10px',
          }}
        >
          <Link
            to="/sign-up"
            style={{ textDecoration: 'none', color: 'white' }}
          >
            Регистрация
          </Link>
        </Button>
      </>
    );
  }

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          {/* Logo */}
          <Box display="flex" alignItems="center">
            <Link
              to="/"
              style={{
                display: 'flex',
                alignItems: 'center',
                color: 'white',
                textDecoration: 'none',
              }}
            >
              <CleaningServicesIcon />
              <Typography
                variant="h6"
                sx={{
                  mr: 2,
                  display: { xs: 'none', md: 'flex' },
                  fontFamily: 'Roboto',
                  letterSpacing: '.25rem',
                  color: 'inherit',
                  textDecoration: 'none',
                  marginLeft: '5px',
                }}
              >
                ДОМ.УСЛУГИ
              </Typography>
            </Link>
          </Box>

          {/* Central buttons mobile (hamburger) */}
          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              <MenuItem onClick={handleCloseNavMenu}>
                <Typography textAlign="center">
                  <Link
                    to="/vacancies"
                    style={{ textDecoration: 'none', color: 'black' }}
                  >
                    Поиск работы
                  </Link>
                </Typography>
              </MenuItem>
              <MenuItem onClick={handleCloseNavMenu}>
                <Typography textAlign="center">
                  <Link
                    to="/resumes"
                    style={{ textDecoration: 'none', color: 'black' }}
                  >
                    Поиск работника
                  </Link>
                </Typography>
              </MenuItem>
            </Menu>
          </Box>

          {/* Central buttons desktop */}
          <Box
            sx={{
              flexGrow: 1,
              display: { xs: 'none', md: 'flex', marginLeft: '0.3rem' },
            }}
          >
            <Button
              onClick={handleCloseNavMenu}
              sx={{ my: 2, display: 'block' }}
            >
              <Link
                to="/vacancies"
                style={{ textDecoration: 'none', color: 'white' }}
              >
                Поиск работы
              </Link>
            </Button>
            <Button
              onClick={handleCloseNavMenu}
              sx={{ my: 2, display: 'block' }}
            >
              <Link
                to="/resumes"
                style={{ textDecoration: 'none', color: 'white' }}
              >
                Поиск работников
              </Link>
            </Button>
          </Box>

          {/* Right buttons */}
          <Box sx={{ flexGrow: 0 }}>{rightButtons}</Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default Navbar;
