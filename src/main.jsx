import { createTheme, ThemeProvider } from '@mui/material';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { blue, grey, purple, red } from '@mui/material/colors';

import App from './App';
import store from './redux';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import './index.css';

const theme = createTheme({
  palette: {
    primary: {
      main: blue[700],
      bluelight: blue[200],
      grey: grey[100],
      darkgrey: grey[500],
    },
    secondary: {
      main: purple[500],
    },
    red: {
      main: red[400],
    },
  },
});

ReactDOM.createRoot(document.getElementById('root')).render(
  // <React.StrictMode>
  <BrowserRouter>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </Provider>
  </BrowserRouter>
  // </React.StrictMode>
);
