import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import Layout from './components/Layout';
import Homepage from './pages/Homepage';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import NotFound from './pages/NotFound';
import Vacancies from './pages/Vacancies';
import Resumes from './pages/Resumes';
import WorkerPage from './pages/WorkerPage';
import EmployerPage from './pages/EmployerPage';
import VacancyPage from './pages/VacancyPage';
import ResumePage from './pages/ResumePage';
import { readRefreshTokenFromLS } from './lib/localStorage';
import { fetchRefreshToken } from './redux/actions/auth';

function App() {
  const dispatch = useDispatch();

  // Авторизация, если в LS лежит refresh токен
  useEffect(() => {
    const refreshToken = readRefreshTokenFromLS();
    if (refreshToken) {
      dispatch(fetchRefreshToken(refreshToken));
    }
  }, [dispatch]);

  // TODO: отсылать refreshToken, если возвращается 401

  return (
    <Routes>
      <Route to="/" element={<Layout />}>
        <Route index element={<Homepage />} />
        <Route path="sign-in" element={<SignIn />} />
        <Route path="sign-up" element={<SignUp />} />
        <Route path="vacancies" element={<Vacancies />} />
        <Route path="resumes" element={<Resumes />} />
        <Route path="workers/:id" element={<WorkerPage />} />
        <Route path="employers/:id" element={<EmployerPage />} />
        <Route path="vacancies/:id" element={<VacancyPage />} />
        <Route path="resumes/:id" element={<ResumePage />} />
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
}

export default App;
