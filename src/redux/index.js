import {
  legacy_createStore as createStore,
  compose,
  applyMiddleware,
} from 'redux';
import createSagaMiddleware from '@redux-saga/core';

import reducer from './reducers/index';
import rootSaga from './sagas/index';

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

export default store;
