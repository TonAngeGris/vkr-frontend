import {
  USER_FAILED,
  USER_RECEIVED,
  USER_REQUESTED,
  USER_MAINDATA_RECEIVED,
  WORKER_CREATED_RESUME_RECEIVED,
  WORKER_UPDATED_RESUME_RECEIVED,
  WORKER_RESUME_DELETED,
  EMPLOYER_CREATED_VACANCY_RECEIVED,
  EMPLOYER_UPDATED_VACANCY_RECEIVED,
  EMPLOYER_VACANCY_DELETED,
  WORKER_PENDING_ASSIGNMENT_CONTRACT_DELETED,
  EMPLOYER_PENDING_ASSIGNMENT_CONTRACT_DELETED,
  EMPLOYER_ASSIGNED_CONTRACT_RECEIVED,
  WORKER_ASSIGNED_CONTRACT_RECEIVED,
  WORKER_IN_PROGRESS_CONTRACT_DELETED,
  EMPLOYER_IN_PROGRESS_CONTRACT_DELETED,
  WORKER_COMPLETED_CONTRACT_RECEIVED,
  EMPLOYER_COMPLETED_CONTRACT_RECEIVED,
  WORKER_PENDING_COMPLETION_CONTRACT_RECEIVED,
  EMPLOYER_PENDING_COMPLETION_CONTRACT_RECEIVED,
  WORKER_PENDING_COMPLETION_CONTRACT_DELETE,
  EMPLOYER_PENDING_COMPLETION_CONTRACT_DELETE,
  WORKER_TOTAL_RATING_RECEIVED,
  EMPLOYER_TOTAL_RATING_RECEIVED,
} from './actions';

export const fetchUserData = (payload) => ({
  type: USER_REQUESTED,
  payload,
});

export const setUserData = (payload) => ({
  type: USER_RECEIVED,
  payload,
});

export const setUserError = (error) => ({
  type: USER_FAILED,
  error,
});

export const setUserMainData = (payload) => ({
  type: USER_MAINDATA_RECEIVED,
  payload,
});

export const setWorkerCreatedResume = (payload) => ({
  type: WORKER_CREATED_RESUME_RECEIVED,
  payload,
});

export const setWorkerUpdatedResume = (payload) => ({
  type: WORKER_UPDATED_RESUME_RECEIVED,
  payload,
});

export const deleteWorkerResume = (payload) => ({
  type: WORKER_RESUME_DELETED,
  payload,
});

export const setEmployerCreatedVacancy = (payload) => ({
  type: EMPLOYER_CREATED_VACANCY_RECEIVED,
  payload,
});

export const setEmployerUpdatedVacancy = (payload) => ({
  type: EMPLOYER_UPDATED_VACANCY_RECEIVED,
  payload,
});

export const deleteEmployerVacancy = (payload) => ({
  type: EMPLOYER_VACANCY_DELETED,
  payload,
});

export const deleteWorkerPendingAssignmentContract = (payload) => ({
  type: WORKER_PENDING_ASSIGNMENT_CONTRACT_DELETED,
  payload,
});

export const deleteEmployerPendingAssignmentContract = (payload) => ({
  type: EMPLOYER_PENDING_ASSIGNMENT_CONTRACT_DELETED,
  payload,
});

export const setEmployerAssignedContract = (payload) => ({
  type: EMPLOYER_ASSIGNED_CONTRACT_RECEIVED,
  payload,
});

export const setWorkerAssignedContract = (payload) => ({
  type: WORKER_ASSIGNED_CONTRACT_RECEIVED,
  payload,
});

export const deleteWorkerInProgressContract = (payload) => ({
  type: WORKER_IN_PROGRESS_CONTRACT_DELETED,
  payload,
});

export const deleteEmployerInProgressContract = (payload) => ({
  type: EMPLOYER_IN_PROGRESS_CONTRACT_DELETED,
  payload,
});

export const setWorkerPendingCompletionContract = (payload) => ({
  type: WORKER_PENDING_COMPLETION_CONTRACT_RECEIVED,
  payload,
});

export const setEmployerPendingCompletionContract = (payload) => ({
  type: EMPLOYER_PENDING_COMPLETION_CONTRACT_RECEIVED,
  payload,
});

export const deleteWorkerPendingCompletionContract = (payload) => ({
  type: WORKER_PENDING_COMPLETION_CONTRACT_DELETE,
  payload,
});

export const deleteEmployerPendingCompletionContract = (payload) => ({
  type: EMPLOYER_PENDING_COMPLETION_CONTRACT_DELETE,
  payload,
});

export const setWorkerCompletedContract = (payload) => ({
  type: WORKER_COMPLETED_CONTRACT_RECEIVED,
  payload,
});

export const setEmployerCompletedContract = (payload) => ({
  type: EMPLOYER_COMPLETED_CONTRACT_RECEIVED,
  payload,
});

export const setWorkerTotalRating = (payload) => ({
  type: WORKER_TOTAL_RATING_RECEIVED,
  payload,
});

export const setEmployerTotalRating = (payload) => ({
  type: EMPLOYER_TOTAL_RATING_RECEIVED,
  payload,
});
