import {
  VACANCIES_FAILED,
  VACANCIES_RECEIVED,
  VACANCIES_REQUESTED,
} from './actions';

export const fetchVacancies = (payload) => ({
  type: VACANCIES_REQUESTED,
  payload,
});

export const setVacanciesData = (payload) => ({
  type: VACANCIES_RECEIVED,
  payload,
});

export const setVacanciesError = (error) => ({
  type: VACANCIES_FAILED,
  error,
});
