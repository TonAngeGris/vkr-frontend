import {
  AUTH_REQUESTED,
  AUTH_RECEIVED,
  AUTH_FAILED,
  AUTH_RESET,
  REFRESH_TOKEN_REQUESTED,
  AUTH_EDITED_RECEIVED,
} from './actions';

export const requestAuthUser = (payload) => ({
  type: AUTH_REQUESTED,
  payload,
});

export const setAuthData = (payload) => ({
  type: AUTH_RECEIVED,
  payload,
});

export const setAuthEditedData = (payload) => ({
  type: AUTH_EDITED_RECEIVED,
  payload,
});

export const setAuthError = (error) => ({
  type: AUTH_FAILED,
  error,
});

export const resetAuthData = () => ({
  type: AUTH_RESET,
});

export const fetchRefreshToken = (payload) => ({
  type: REFRESH_TOKEN_REQUESTED,
  payload,
});
