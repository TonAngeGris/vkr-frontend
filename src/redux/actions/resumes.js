import { RESUMES_FAILED, RESUMES_RECEIVED, RESUMES_REQUESTED } from './actions';

export const fetchResumes = (payload) => ({
  type: RESUMES_REQUESTED,
  payload,
});

export const setResumesData = (payload) => ({
  type: RESUMES_RECEIVED,
  payload,
});

export const setResumesError = (error) => ({
  type: RESUMES_FAILED,
  error,
});
