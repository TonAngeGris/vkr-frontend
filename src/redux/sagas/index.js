import { spawn } from 'redux-saga/effects';

import authSagaWatcher from './authSaga';
import resumesSagaWatcher from './resumesSaga';
import vacanciesSagaWatcher from './vacanciesSaga';
import userSagaWatcher from './userSaga';

export default function* rootSaga() {
  yield spawn(authSagaWatcher);
  yield spawn(vacanciesSagaWatcher);
  yield spawn(resumesSagaWatcher);
  yield spawn(userSagaWatcher);
}
