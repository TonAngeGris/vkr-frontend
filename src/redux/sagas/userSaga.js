import { call, put, takeEvery } from 'redux-saga/effects';

import {
  getEmployerContracts,
  getEmployerData,
  getEmployerVacancies,
} from '../../api/employer';
import {
  getWorkerData,
  getWorkerContracts,
  getWorkerResumes,
} from '../../api/worker';
import { USER_REQUESTED } from '../actions/actions';
import { setUserData, setUserError } from '../actions/user';

function* fetchUserHandler(action) {
  try {
    const { userGroup, id } = action.payload;

    let data = {};

    if (userGroup === 'workers') {
      const workerData = yield call(getWorkerData, id);
      const workerContracts = yield call(getWorkerContracts, id);
      const workerResumes = yield call(getWorkerResumes, id);

      data = {
        workerData,
        workerContracts,
        workerResumes,
      };
    }

    if (userGroup === 'employers') {
      const employerData = yield call(getEmployerData, id);
      const employerContracts = yield call(getEmployerContracts, id);
      const employerVacancies = yield call(getEmployerVacancies, id);

      data = {
        employerData,
        employerContracts,
        employerVacancies,
      };
    }

    yield put(setUserData(data));
  } catch (error) {
    yield put(setUserError(error.message));
  }
}

export default function* userSagaWatcher() {
  yield takeEvery(USER_REQUESTED, fetchUserHandler);
}
