import { call, put, spawn, takeEvery } from 'redux-saga/effects';

import { addAccessTokenToHeaders } from '../../api/api';
import { authUser, refresh, whoIAm } from '../../api/auth';
import {
  addRefreshTokenToLS,
  addUserTypeToLS,
  readRefreshTokenFromLS,
  readUserTypeFromLS,
} from '../../lib/localStorage';
import { AUTH_REQUESTED, REFRESH_TOKEN_REQUESTED } from '../actions/actions';
import { setAuthData, setAuthError } from '../actions/auth';

function tokenHandler(tokenData) {
  const accessToken = tokenData.access_token;
  const refreshToken = tokenData.refresh_token;

  addAccessTokenToHeaders(accessToken);
  addRefreshTokenToLS(refreshToken);
}

function* refreshTokenHandler() {
  try {
    const refreshToken = yield call(readRefreshTokenFromLS);
    const userGroup = yield call(readUserTypeFromLS);

    const tokenData = yield call(refresh, refreshToken, userGroup);

    yield call(tokenHandler, tokenData);

    const userData = yield call(whoIAm, userGroup);

    yield put(setAuthData(userData));
  } catch (err) {
    yield put(setAuthError(err.message));
  }
}
function* authHandler(action) {
  try {
    const { userGroup, authAction, ...payload } = action.payload;

    const tokenData = yield call(authUser, payload, userGroup, authAction);

    yield call(tokenHandler, tokenData);
    yield call(addUserTypeToLS, userGroup);
    const userData = yield call(whoIAm, userGroup);

    yield put(setAuthData(userData));
  } catch (err) {
    const errors =
      err.response.data?.errors ||
      err.response.data?.message ||
      err.response.data?.error;
    yield put(setAuthError(errors));
  }
}

function* authWatcher() {
  yield takeEvery(AUTH_REQUESTED, authHandler);
}

function* refreshTokenWatcher() {
  yield takeEvery(REFRESH_TOKEN_REQUESTED, refreshTokenHandler);
}

export default function* authSagaWatcher() {
  yield spawn(authWatcher);
  yield spawn(refreshTokenWatcher);
}
