import { call, put, takeEvery } from '@redux-saga/core/effects';

import { getResumes } from '../../api/resume';
import { RESUMES_REQUESTED } from '../actions/actions';
import { setResumesData, setResumesError } from '../actions/resumes';

function* fetchResumesHandler(action) {
  try {
    const { resumes: items, resumeAmount: allItemsNumber } = yield call(
      getResumes,
      action.payload
    );

    yield put(setResumesData({ items, allItemsNumber }));
  } catch (error) {
    yield put(setResumesError(error.message));
  }
}

export default function* resumesSagaWatcher() {
  yield takeEvery(RESUMES_REQUESTED, fetchResumesHandler);
}
