import { call, put, takeEvery } from '@redux-saga/core/effects';

import { getVacancies } from '../../api/vacancy';
import { VACANCIES_REQUESTED } from '../actions/actions';
import { setVacanciesData, setVacanciesError } from '../actions/vacancies';

function* fetchVacanciesHandler(action) {
  try {
    const { vacancies: items, vacancyAmount: allItemsNumber } = yield call(
      getVacancies,
      action.payload
    );

    yield put(setVacanciesData({ items, allItemsNumber }));
  } catch (error) {
    yield put(setVacanciesError(error.message));
  }
}

export default function* vacanciesSagaWatcher() {
  yield takeEvery(VACANCIES_REQUESTED, fetchVacanciesHandler);
}
