import {
  RESUMES_FAILED,
  RESUMES_RECEIVED,
  RESUMES_REQUESTED,
} from '../actions/actions';

const initialState = {
  items: [],
  allItemsNumber: 0,
  fetching: false,
  error: null,
};

// eslint-disable-next-line default-param-last
export default function resumesReducer(state = initialState, action) {
  switch (action.type) {
    case RESUMES_REQUESTED:
      return {
        ...state,
        items: [],
        allItemsNumber: 0,
        fetching: true,
        eror: null,
      };

    case RESUMES_RECEIVED:
      return {
        ...state,
        items: action.payload.items,
        allItemsNumber: action.payload.allItemsNumber,
        fetching: false,
        eror: null,
      };

    case RESUMES_FAILED:
      return {
        ...state,
        items: [],
        allItemsNumber: 0,
        fetching: false,
        eror: action.error,
      };

    default:
      return state;
  }
}
