import {
  USER_FAILED,
  USER_MAINDATA_RECEIVED,
  USER_RECEIVED,
  USER_REQUESTED,
  WORKER_CREATED_RESUME_RECEIVED,
  WORKER_UPDATED_RESUME_RECEIVED,
  WORKER_RESUME_DELETED,
  EMPLOYER_CREATED_VACANCY_RECEIVED,
  EMPLOYER_UPDATED_VACANCY_RECEIVED,
  EMPLOYER_VACANCY_DELETED,
  WORKER_PENDING_ASSIGNMENT_CONTRACT_DELETED,
  EMPLOYER_PENDING_ASSIGNMENT_CONTRACT_DELETED,
  WORKER_ASSIGNED_CONTRACT_RECEIVED,
  EMPLOYER_ASSIGNED_CONTRACT_RECEIVED,
  WORKER_IN_PROGRESS_CONTRACT_DELETED,
  WORKER_COMPLETED_CONTRACT_RECEIVED,
  WORKER_PENDING_COMPLETION_CONTRACT_RECEIVED,
  WORKER_PENDING_COMPLETION_CONTRACT_DELETE,
  EMPLOYER_PENDING_COMPLETION_CONTRACT_DELETE,
  EMPLOYER_COMPLETED_CONTRACT_RECEIVED,
  EMPLOYER_IN_PROGRESS_CONTRACT_DELETED,
  EMPLOYER_PENDING_COMPLETION_CONTRACT_RECEIVED,
  WORKER_TOTAL_RATING_RECEIVED,
  EMPLOYER_TOTAL_RATING_RECEIVED,
} from '../actions/actions';

const initialState = {
  userData: {},
  fetching: false,
  error: null,
};

// eslint-disable-next-line default-param-last
export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case USER_REQUESTED:
      return {
        ...state,
        userData: {},
        fetching: true,
        eror: null,
      };

    case USER_RECEIVED:
      return {
        ...state,
        userData: action.payload,
        fetching: false,
        eror: null,
      };

    case USER_FAILED:
      return {
        ...state,
        userData: {},
        fetching: false,
        eror: action.error,
      };

    case USER_MAINDATA_RECEIVED:
      return {
        ...state,
        userData: {
          ...state.userData,
          [`${action.payload.userGroup}Data`]: { ...action.payload.userData },
        },
        fetching: false,
        eror: null,
      };

    case WORKER_CREATED_RESUME_RECEIVED:
      return {
        ...state,
        userData: {
          ...state.userData,
          workerResumes: [
            action.payload.createdResume,
            ...state.userData.workerResumes,
          ],
        },
        fetching: false,
        eror: null,
      };

    case WORKER_UPDATED_RESUME_RECEIVED: {
      const newWorkerResumes = state.userData.workerResumes.map((item) =>
        item.id === action.payload.id ? action.payload.updatedResume : item
      );

      return {
        ...state,
        userData: {
          ...state.userData,
          workerResumes: [...newWorkerResumes],
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_RESUME_DELETED: {
      const newWorkerResumes = state.userData.workerResumes.filter(
        (item) => item.id !== action.payload.id
      );

      return {
        ...state,
        userData: {
          ...state.userData,
          workerResumes: [...newWorkerResumes],
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_CREATED_VACANCY_RECEIVED:
      return {
        ...state,
        userData: {
          ...state.userData,
          employerVacancies: [
            action.payload.createdVacancy,
            ...state.userData.employerVacancies,
          ],
        },
        fetching: false,
        eror: null,
      };

    case EMPLOYER_UPDATED_VACANCY_RECEIVED: {
      const newEmployerVacancies = state.userData.employerVacancies.map(
        (item) =>
          item.id === action.payload.id ? action.payload.updatedVacancy : item
      );

      return {
        ...state,
        userData: {
          ...state.userData,
          employerVacancies: [...newEmployerVacancies],
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_VACANCY_DELETED: {
      const newEmployerVacancies = state.userData.employerVacancies.filter(
        (item) => item.id !== action.payload.id
      );

      return {
        ...state,
        userData: {
          ...state.userData,
          employerVacancies: [...newEmployerVacancies],
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_PENDING_ASSIGNMENT_CONTRACT_DELETED: {
      const newWorkerPendingContracts =
        state.userData.workerContracts.pending_assignment.filter(
          (item) => item.id !== action.payload.id
        );

      return {
        ...state,
        userData: {
          ...state.userData,
          workerContracts: {
            ...state.userData.workerContracts,
            pending_assignment: [...newWorkerPendingContracts],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_PENDING_ASSIGNMENT_CONTRACT_DELETED: {
      const newEmployerPendingContracts =
        state.userData.employerContracts.pending_assignment.filter(
          (item) => item.id !== action.payload.id
        );

      return {
        ...state,
        userData: {
          ...state.userData,
          employerContracts: {
            ...state.userData.employerContracts,
            pending_assignment: [...newEmployerPendingContracts],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_ASSIGNED_CONTRACT_RECEIVED: {
      return {
        ...state,
        userData: {
          ...state.userData,
          workerContracts: {
            ...state.userData.workerContracts,
            in_progress: [
              ...state.userData.workerContracts.in_progress,
              action.payload.assignedContract,
            ],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_ASSIGNED_CONTRACT_RECEIVED: {
      return {
        ...state,
        userData: {
          ...state.userData,
          employerContracts: {
            ...state.userData.employerContracts,
            in_progress: [
              ...state.userData.employerContracts.in_progress,
              action.payload.assignedContract,
            ],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_IN_PROGRESS_CONTRACT_DELETED: {
      const newWorkerInProgressContracts =
        state.userData.workerContracts.in_progress.filter(
          (item) => item.id !== action.payload.id
        );

      return {
        ...state,
        userData: {
          ...state.userData,
          workerContracts: {
            ...state.userData.workerContracts,
            in_progress: [...newWorkerInProgressContracts],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_IN_PROGRESS_CONTRACT_DELETED: {
      const newEmployerInProgressContracts =
        state.userData.employerContracts.in_progress.filter(
          (item) => item.id !== action.payload.id
        );

      return {
        ...state,
        userData: {
          ...state.userData,
          employerContracts: {
            ...state.userData.employerContracts,
            in_progress: [...newEmployerInProgressContracts],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_PENDING_COMPLETION_CONTRACT_RECEIVED: {
      return {
        ...state,
        userData: {
          ...state.userData,
          workerContracts: {
            ...state.userData.workerContracts,
            pending_completion: [
              ...state.userData.workerContracts.pending_completion,
              action.payload.completedContract,
            ],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_PENDING_COMPLETION_CONTRACT_RECEIVED: {
      return {
        ...state,
        userData: {
          ...state.userData,
          employerContracts: {
            ...state.userData.employerContracts,
            pending_completion: [
              ...state.userData.employerContracts.pending_completion,
              action.payload.completedContract,
            ],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_PENDING_COMPLETION_CONTRACT_DELETE: {
      const newWorkerPendingCompletionContracts =
        state.userData.workerContracts.pending_completion.filter(
          (item) => item.id !== action.payload.id
        );

      return {
        ...state,
        userData: {
          ...state.userData,
          workerContracts: {
            ...state.userData.workerContracts,
            pending_completion: [...newWorkerPendingCompletionContracts],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_PENDING_COMPLETION_CONTRACT_DELETE: {
      const newEmployerPendingCompletionContracts =
        state.userData.employerContracts.pending_completion.filter(
          (item) => item.id !== action.payload.id
        );

      return {
        ...state,
        userData: {
          ...state.userData,
          employerContracts: {
            ...state.userData.employerContracts,
            pending_completion: [...newEmployerPendingCompletionContracts],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_COMPLETED_CONTRACT_RECEIVED: {
      return {
        ...state,
        userData: {
          ...state.userData,
          workerContracts: {
            ...state.userData.workerContracts,
            completed: [
              ...state.userData.workerContracts.completed,
              action.payload.completedContract,
            ],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case EMPLOYER_COMPLETED_CONTRACT_RECEIVED: {
      return {
        ...state,
        userData: {
          ...state.userData,
          employerContracts: {
            ...state.userData.employerContracts,
            completed: [
              ...state.userData.employerContracts.completed,
              action.payload.completedContract,
            ],
          },
        },
        fetching: false,
        eror: null,
      };
    }

    case WORKER_TOTAL_RATING_RECEIVED:
      return {
        ...state,
        userData: {
          ...state.userData,
          workerData: {
            ...state.userData.workerData,
            rating: action.payload.rating,
          },
        },
        fetching: false,
        eror: null,
      };

    case EMPLOYER_TOTAL_RATING_RECEIVED:
      return {
        ...state,
        userData: {
          ...state.userData,
          employerData: {
            ...state.userData.employerData,
            rating: action.payload.rating,
          },
        },
        fetching: false,
        eror: null,
      };

    default:
      return state;
  }
}
