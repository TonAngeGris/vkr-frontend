import {
  VACANCIES_FAILED,
  VACANCIES_RECEIVED,
  VACANCIES_REQUESTED,
} from '../actions/actions';

const initialState = {
  items: [],
  allItemsNumber: 0,
  fetching: false,
  error: null,
};

// eslint-disable-next-line default-param-last
export default function vacanciesReducer(state = initialState, action) {
  switch (action.type) {
    case VACANCIES_REQUESTED:
      return {
        ...state,
        items: [],
        allItemsNumber: 0,
        fetching: true,
        eror: null,
      };

    case VACANCIES_RECEIVED:
      return {
        ...state,
        items: action.payload.items,
        allItemsNumber: action.payload.allItemsNumber,
        fetching: false,
        eror: null,
      };

    case VACANCIES_FAILED:
      return {
        ...state,
        items: [],
        allItemsNumber: 0,
        fetching: false,
        eror: action.error,
      };

    default:
      return state;
  }
}
