import {
  AUTH_REQUESTED,
  AUTH_RECEIVED,
  AUTH_FAILED,
  AUTH_RESET,
  REFRESH_TOKEN_REQUESTED,
  AUTH_EDITED_RECEIVED,
} from '../actions/actions';

const initialState = {
  authUser: {},
  fetching: false,
  error: null,
};

// eslint-disable-next-line default-param-last
export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case REFRESH_TOKEN_REQUESTED:
    case AUTH_REQUESTED:
      return {
        ...state,
        authUser: {},
        fetching: true,
        error: null,
      };

    case AUTH_RECEIVED:
    case AUTH_EDITED_RECEIVED:
      return {
        ...state,
        authUser: action.payload,
        fetching: false,
        error: null,
      };

    case AUTH_FAILED:
      return {
        ...state,
        authUser: {},
        fetching: false,
        error: action.error,
      };

    case AUTH_RESET:
      return {
        ...state,
        authUser: {},
        fetching: false,
        error: null,
      };

    default:
      return state;
  }
}
