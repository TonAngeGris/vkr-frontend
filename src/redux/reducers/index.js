import { combineReducers } from 'redux';

import authReducer from './authReducer';
import vacanciesReducer from './vacanciesReducer';
import resumesReducer from './resumesReducer';
import userReducer from './userReducer';

const reducer = combineReducers({
  authReducer,
  vacanciesReducer,
  resumesReducer,
  userReducer,
});

export default reducer;
